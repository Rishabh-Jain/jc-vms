'use strict'

const { Command } = require('@adonisjs/ace')
const Setting = use ('App/Models/Setting')
const { Disk } = use('App/Help');

class Install extends Command {
    
    static get signature () {
        return 'setup'
    }
  
    static get description () {
        return 'Install the server client.'
    }

    async handle (args, options) {
        this.info("Installing encoding server.")
        Setting.create({name: "diskspace", value: Disk.total})
        Setting.create({name: "diskfree", value: Disk.free})
        Setting.create({name: "diskused", value: Disk.used})
        this.info("Installed.")
    }
}

module.exports = Install
