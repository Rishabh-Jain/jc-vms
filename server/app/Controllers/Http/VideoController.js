'use strict'
const path = require( 'path' )
const fs = require( 'fs' )

const { aR, aRError, videosPath } = use( 'App/Help' )
const Helpers = use( 'Helpers' )
const Video = use( 'App/Models/Video' )
const Link = use( 'App/Models/Link' )
const Event = use( 'Event' )
const Log = use( 'Logger' )
const { validate } = use( 'Validator' )

const rs = require( 'randomstring' )
const md5 = require( 'md5-file' )
const send = require("send")

class VideoController {

    async link ({ request }) {
        try {
            const hash = request.params.hash
            const video = await Video.query().where('hash', '=', hash).withCount('links').with('links').first()

            if (!video) {
                throw new Error("Invalid link.")
            }

            if (!video.processed) {
                throw new Error("Video is still under processing. Please wait.")
            }

            const $log = `Video ${video.id} : ${video.hash}`
            const $count = video.links_count

            const $links = {}

            if ($count > 0) {
                Log.info(`${$log} Sending Existsing links`)
                const $dat = await video.links().fetch()
                const $data = $dat.rows
                for (let i = 0; i < $data.length; i++) {
                    if (!$links[$data[i].quality]) {
                        $links[$data[i].quality] = {
                          quality: $data[i].quality
                        }
                    }
                    $links[$data[i].quality][$data[i].type] = `/video/${$data[i].type}/${$data[i].link}.mp4`
                }
                return aR({ image: `/image/${video.hash}.png`, video: $links })
            }

            const $encodings = JSON.parse(video.encodings)

            for (let i = 0; i < $encodings.length; i++) {

                const $play = rs.generate(32)
                const $download = rs.generate(32)

                await Link.create({ video_id: video.id, quality: $encodings[i].name, link: $play, type: 'play' })
                await Link.create({ video_id: video.id, quality: $encodings[i].name, link: $download, type: 'download' })

                $links[$encodings[i].name] = {
                    quality: $encodings[i].name,
                    play : `/video/play/${$play}.mp4`,
                    download : `/video/download/${$download}.mp4`
                }
            }

            Log.info(`${$log} sending generated links.`)
            return aR({ image: `/image/${video.hash}.png`, video: $links})

        } catch(e) {
            Log.error(e)
            return aRError(e.message)
        }
    }

    async store ({ request }) {

        const $video = request.file('video', {
            types: [ 'video' ],
            allowedExtensions: [ 'mp4' ]
        })
        try {
            const validation  = await validate(request.all(), {
                'id' : 'required|min:1|unique:videos,video_id',
                'encode': 'required',
                'encodings': 'required_when:encode,1',
                'callback': 'required'
            })

            let encode = request.input('encode')

            if (encode == true) {
                encode = true
            } else if (encode == false) {
                encode = false
            } else {
                throw Error("Encoding Validation failed. Please try again")
            }

            if (validation.fails()) {
                throw Error("Validation failed. Please try again. ")

            }

            if (!$video) {
                throw Error('Vo video has been provided.')
            }

            let $path = 'raw'

            if (encode) {
                $path = 'raw'
            } else {
                $path = 'finished'
            }
            await $video.move(videosPath($path))

            if (!$video.moved()) {
                throw Error($video.error())
            }

            const $file = path.resolve($video._location, $video.fileName)
            const hash = await md5.sync($file)
            const link = `/videos/link/${hash}/`
            const encodings = JSON.parse(request.input('encodings'))

            if (encode) {
                for (let i = 0; i < encodings.length; i++) {
                    encodings[i].name = encodings[i].name.split("_")[0]
                }
            }

            const vidio = await Video.create({
                'hash' : hash,
                'video_id': request.input('id'),
                'link' : link,
                'location' : $file,
                'encode': encode,
                'encodings': JSON.stringify(encodings),
                'callback': request.input('callback'),
                'processed' : !encode
            })

            Event.fire("video::new", vidio)

            return aR({
                'link': link,
                'status': !encode
            })

        } catch (error) {
            Log.error(error)
            if ($video.moved()) {
                const $file = path.resolve($video._location, $video.fileName)
                await fs.unlinkSync($file)
            }

            return aRError(error.message)
        }
    }

    async img ({request, response}) {
        const hash = request.params.hash
        const $path = videosPath(`images/${hash}.png`)
        if (fs.existsSync($path)) {
            return response.download($path)
        }
        return response.type('image/png').download(videosPath('images/default.png'))
        // return aRError("Something went wrong.")
    }

    async action ({request, response}) {
        try {
            const hash = request.params.hash
            const type = request.params.type

            if (type !== 'play' && type !== 'download') {
                throw new Error("Invalid link type")
            }

            if (type === 'play') {
                if (!request.header('range') || !request.header('Range')) {
                    throw new Error("Invalid link")
                }
            }

            const $link = await Link.query().where('link', '=', hash).with('video').first()

            if ($link) {
                const video = await $link.video().fetch()
                const videoHash = video.hash
                const file = videosPath(`finished/${$link.quality}-${videoHash}.mp4`)
                if ($link.type === 'play') {
                    response.type('video/mp4')
                    return response.download(file)
                }
            }

            throw new Error("Invalid link.")

        } catch(e) {
            Log.error(e)
            return aRError(e.message)
        }
    }

    async destroy({request}) {
        try {
            const hash = request.params.hash
            const $video = await Video.query().where('hash', '=', hash).first()
            const $locations = JSON.parse($video.location)

            $locations.forEach(location => {
                if (fs.existsSync(location.location)) {
                    fs.unlinkSync(location.location)
                }
            })
            await $video.delete()
            return aR({code: 200, status: 'ok', data: `Video ${$video.id}deleted successfully.`})

        } catch(e) {
            Log.error(e)
            aRError(e.message)
        }
    }
}

module.exports = VideoController
