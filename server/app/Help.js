const path = require('path')
const disk = require('diskusage')


const aR = function ($response = {}, $code = 200) {
    return {
        status : 'ok',
        code : $code,
        data : $response
    }
}

const aRError = function ($error = "Something went wrong", $code = 400) {
    return {
        status  :   'error',
        code    :   $code,
        error   :   $error
    }
}

const appPath = function ($path = '') {
    const path = require('path')
    return path.join(__dirname, '../', $path)
}

const storagePath = function ($path = '') {
    return appPath(path.join("storage", $path))
}

const videosPath = function ($path = '') {
    return storagePath(path.join("videos", $path))
}


const Disk = function () {
    try {
        const devider = 1048576 //Bytes to MB 1024 * 1024

        let info = disk.checkSync(__dirname)

        const total = Math.round(info.total / devider)
        const free = Math.round(info.free / devider)
        const usage = total - free

        return {
            total : total,
            free: free,
            usage: usage
        }
    }
    catch (err) {
        Log.error(err)
        return {
            total : false,
            free : false,
            usage : false,
        }
    }

}

module.exports = {
    aR, aRError, appPath, storagePath, videosPath, Disk
}
