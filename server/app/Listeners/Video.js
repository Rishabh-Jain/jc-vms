'use strict'

const Video = exports = module.exports = {}

const ffmpeg = require('fluent-ffmpeg')
const fs = require('fs')
const axios = require('axios')

const { videosPath, Disk } = use('App/Help')
const V = use('App/Models/Video')
const Setting = use('App/Models/Setting')
const Log = use('Logger')
const Env = use('Env')
const Event = use( 'Event' )

Video.created = async (video) => {
    const $path = video.location
    try {
        const $encodings = JSON.parse(video.encodings)
        const $videos = []
        const $img = videosPath(`images`)
        
        await ffmpeg($path)
            .screenshots({
                timestamps: ['20%'],
                filename: `${video.hash}.png`,
                folder: $img,
                size: '1920x1080'
            });

        $encodings.forEach($encoding => {

            console.log($encoding.name, 'staterd')
            const qty = $encoding.name.split('_')[0]
            const $encodedPath = videosPath(`encoded/${qty}-${video.hash}.mp4`)
            const $output = videosPath(`finished/${qty}-${video.hash}.mp4`)

            const $promise = new Promise ((resolve, reject) => {
                ffmpeg($path)
                // .audioCodec($encoding.audio_codec)
                .audioBitrate($encoding.audio_bit_rate)
                .fps($encoding.quality)
                .size(`${$encoding.width}x${$encoding.height}`)
                .on('progress', (progress) => {
                    console.log("Video", video.id, $encoding.name, progress.percent)
                }).on('end', () => {
                    console.log('Transcoding succeeded !')
                    Log.info("Video Encoded", {id: video.id, encoding: qty})
                    resolve({
                        'encoding' : $encoding,
                        'location' : $output,
                        'encoded': $encodedPath,
                        'qty' : qty
                    })
                }).on('error', (err) => {
                    console.log(err)
                    reject(err)
                }).save($encodedPath)
            })

            $videos.push($promise)
        })

        Promise.all($videos)
            .then(async (vid) => {

                const $locations = []
                fs.unlinkSync($path)
                vid.forEach(data => {
                    $locations.push({qty: data.qty, location: data.location})
                    fs.renameSync(data.encoded, data.location)
                })

                await V.query().where('id', '=', video.id).update({
                    location: JSON.stringify($locations),
                    processed: false
                })

                Event.fire('video::encoded', video)

            }).catch( err => {
                Log.error(err)
            })

    } catch (e) {
        console.log(e)
        Log.error(e)
    }


}


Video.encoded = async (video) => {

    console.log("Calling", video.callback)

    video.processed = true;
    await video.save();

    Setting.query().where('name', '=', 'diskusage').update({value: Disk.usage})
    Setting.query().where('name', '=', 'diskfree').update({value: Disk.free})

    const params = {
        'hash': video.hash,
        'diskusage': Disk().usage,
        'diskfree': Disk().free
    }
    
    axios.post(video.callback, params, {headers: {'JNEX-UID': Env.get('APP_ID')}})
    .then(res => {
        Log.info(res.data)
    }).catch(err => {
        Log.error(err)
    })
}
