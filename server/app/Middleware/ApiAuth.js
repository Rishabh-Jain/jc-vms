'use strict'

const env = use('Env');
const { aRError } = require('../Help')

class ApiAuth {

    async handle ({ request, response }, next) {
        if (
            request.header('JNEX-UID') !== env.get('APP_ID') ||
            request.header('JNEX-SECRET') !== env.get('APP_SECRET')
        ) {
            return response.send(aRError("Invalid reuqest", 401))
        }
        await next()
    }
}

module.exports = ApiAuth
