'use strict'

const Model = use('Model')

class Link extends Model {
    video () {
        return this.belongsTo("App/Models/Video")
    }
}

module.exports = Link
