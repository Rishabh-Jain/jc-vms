'use strict'

const Model = use('Model')

class Video extends Model {
    
    links () {
        return this.hasMany("App/Models/Link")
    }
}

module.exports = Video
