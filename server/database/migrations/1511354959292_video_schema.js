'use strict'

const Schema = use('Schema')

class VideoSchema extends Schema {
  up () {
    this.create('videos', (table) => {
        table.increments()
        table.string('hash')
        table.integer('video_id').unique()
        table.text('link').nullable()
        table.boolean('encode')
        table.boolean('processed')
        table.text('encodings')
        table.text('location')
        table.text('callback')

        table.timestamps()
    })
  }

  down () {
    this.drop('videos')
  }
}

module.exports = VideoSchema
