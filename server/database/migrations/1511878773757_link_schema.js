'use strict'

const Schema = use('Schema')

class LinkSchema extends Schema {
  up () {
    this.create('links', (table) => {
      table.increments()
      table.integer('video_id').unsigned()
      table.string('quality')
      table.text('link')
      table.string('type')
      table.timestamps()

      table.foreign('video_id').references('id').on('videos').onDelete('cascade')
    })
  }

  down () {
    this.drop('links')
  }
}

module.exports = LinkSchema
