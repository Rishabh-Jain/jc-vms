const Event = use('Event')

Event.on('video::new', 'Video.created')
Event.on('video::encoded', 'Video.encoded')
