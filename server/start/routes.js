'use strict'

const Route = use('Route')

Route.post('/videos', 'VideoController.store').middleware('api')
Route.get('/videos/link/:hash', "VideoController.link").middleware('api')
Route.delete('/videos/link/:hash', "VideoController.destroy").middleware('api')

Route.get('/video/:type/:hash.mp4', "VideoController.action")
Route.get('/image/:hash.png', "VideoController.img")
