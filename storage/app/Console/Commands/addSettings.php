<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class addSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:settings
                                {--A|all : Add All settings.}
                                {--P|profile : Add profile settings.}
                                {--G|general : Add general settings}
                                {--F|firewall : Add firewall settings}
                                {--L|player : Add player settings}
                                {--D|ad : Add advertisements settings.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Default settings in the server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Adding settings.");
        $settings = collect([

            // General Settings
            [ 'name' => 'website_url',                  'value' => 'localhost',                     'type' => 'general' ],
            [ 'name' => 'admin_url',                    'value' => 'admin',                         'type' => 'general' ],
            [ 'name' => 'embed_slug',                   'value' => 'embed_slug',                    'type' => 'general' ],
            [ 'name' => 'website_title',                'value' => 'Juicycodes Storage',            'type' => 'general' ],
            [ 'name' => 'theme',                        'value' => 'blue2',                         'type' => 'general' ],
            [ 'name' => 'sync_jobs',                    'value' => '2',                             'type' => 'general' ],

            // Profile Settings
            [ 'name' => 'default_priview_link',         'value' => '{ASSET}/no-preview.png',        'type' => 'profile' ],
            [ 'name' => 'default_video_link',           'value' => '{ASSET}/no-preview.png',        'type' => 'profile' ],
            [ 'name' => 'items_per_page',               'value' => 15,                              'type' => 'profile' ],
            [ 'name' => 'video_sub_titles',             'value' => true,                            'type' => 'profile' ],
            [ 'name' => 'video_sub_titles_autostart',   'value' => false,                           'type' => 'profile' ],

            // Advertisement Settings
            [ 'name' => 'popup_ad',                     'value' => false,                           'type' => 'ad' ],
            [ 'name' => 'banner_ad',                    'value' => false,                           'type' => 'ad' ],
            [ 'name' => 'popup_ad_code',                'value' => "",                              'type' => 'ad' ],
            [ 'name' => 'banner_ad_code',               'value' => "",                              'type' => 'ad' ],

            // Firewall Settings
            [ 'name' => 'firewall_restriction',         'value' => false,                           'type' => 'firewall' ],
            [ 'name' => 'firewall_countries',           'value' => json_encode(['AF']),             'type' => 'firewall' ],
            [ 'name' => 'firewall_allowed_domains',     'value' => '',                              'type' => 'firewall' ],
            [ 'name' => 'firewall_blocked_ips',         'value' => '',                              'type' => 'firewall' ],

            // Video Players Settings

            // VideoJS Player Settings
            [ 'name' => 'video_player',                 'value' => 'videojs',                       'type' => 'player', ],
            [ 'name' => 'player_width',                 'value' => '100%',                          'type' => 'player', ],
            [ 'name' => 'player_height',                'value' => '100%',                          'type' => 'player', ],
            [ 'name' => 'subtitle_font_size',           'value' => '14px',                          'type' => 'player', ],
            [ 'name' => 'subtitle_font_color',          'value' => '#F5F06D',                       'type' => 'player', ],
            [ 'name' => 'subtitle_font_family',         'value' => 'Trebuchet MS, Sans Serif',      'type' => 'player', ],
            [ 'name' => 'subtitle_background_color',    'value' => '',                              'type' => 'player', ],
            [ 'name' => 'aspect_ratio',                 'value' => '',                              'type' => 'player', ],
            [ 'name' => 'player_controls',              'value' => false,                           'type' => 'player', ],
            [ 'name' => 'video_autostart',              'value' => false,                           'type' => 'player', ],
            [ 'name' => 'player_rendering_mode',        'value' => 'html',                          'type' => 'player' ],

            //JWPlayer Settings
            [ 'name' => 'jwplayer_licence_key',         'value' => encrypt(env('JWPLAYER_KEY')),    'type' => 'player',       'sub_type' => 'jwplayer' ],
            [ 'name' => 'jwplayer_about_text',          'value' => '',                              'type' => 'player',       'sub_type' => 'jwplayer' ],
            [ 'name' => 'jwplayer_about_link',          'value' => '',                              'type' => 'player',       'sub_type' => 'jwplayer' ],
            [ 'name' => 'jwplayer_share_button',        'value' => false,                           'type' => 'player',       'sub_type' => 'jwplayer' ],

        ]);

        $all = $this->option('all');
        if (!$this->option('ad') && !$this->option('profile') && !$this->option('general') && !$this->option('firewall') && !$this->option('player')) {
            $all = true;
        }

        $settings->map(function ($setting) use ($all) {
            if ($all) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            } else if ($setting['type'] === 'general' && $this->option('general')) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            } else if ($setting['type'] === 'profile' && $this->option('profile')) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            } else if ($setting['type'] === 'firewall' && $this->option('firewall')) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            } else if ($setting['type'] === 'player' && $this->option('player')) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            } else if ($setting['type'] === 'ad' && $this->option('ad')) {
                \App\Setting::create($setting);
                $this->info("Added {$setting['name']}");
            }
        });
    }
}
