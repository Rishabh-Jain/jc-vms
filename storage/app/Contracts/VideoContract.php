<?php
namespace App\Contracts;

final class VideoContract {

    public static $type = [
        'encode' => 'encoding',
        'create' => 'created',
        'update' => 'updated',
        'delete' => 'deleted'
    ];

    final public static function type ($key) {
        if (array_key_exists ($key, static::$type)) {
            return static::$type[$key];
        } else {
            return 'video';
        }
    }
}
