<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLog;
use App\VideoView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        $videoViews = VideoView::orderByDesc('id')->select('created_at as date', 'views')->limit(10)->get();
        $videoViews->map(function ($view) {
            $view->date = \Carbon\Carbon::parse($view->date)->toFormattedDateString();
        });
        return v("home", ['views' => $videoViews->toJson()]);
    }

    public function users()
    {
        // dd(config('theme.name'));
        $users = User::orderByDesc('created_at')
            ->paginate(config('settings.items_per_page', 15));
        return v('users.list', ['users' => $users]);
    }

    public function userLogs()
    {
        $logs = UserLog::orderByDesc('created_at')
            ->with('user')
            ->paginate(config('settings.items_per_page', 15));

        return v('users.logs', ['userLogs' => $logs]);
    }

    public function profile(User $user)
    {
        if ($user->id === 1) {
            return back();
        }
        return v('users.profile', ['user' => $user]);
    }

    public function updateUser(Request $request, User $user)
    {
        if ($user->id === 1) {
            return back();
        }

        $rules = [
             'name' => 'min:2',
             'email' => 'email',
        ];

        if ($request->input('password')) {
            $rules['password'] = 'min:6';
        }
        $request->validate($rules);

        if (Auth::attempt(['password' => $request->input('password_old'), 'email' => $user->email])) {
            var_dump($request->all());
            dd("missmatch");
            return back()->withErrors(['password_old' => 'Invalid password']);
        }

        $user->update($request->only(array_keys($rules)));
        return back()->flash("Updated sucessfully.");
    }
}
