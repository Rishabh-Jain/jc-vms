<?php

namespace App\Http\Controllers;

use App\ServerLog;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Server;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servers = Server::orderByDesc('created_at')
            ->with('user')
            ->paginate(config('settings.items_per_page', 15));
        return v("server.list", ['servers' => $servers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return v("server.add");
    }

    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required|min:5',
            'uid'       => 'required|size:64|unique:servers',
            'secret'    => 'required|size:64|unique:servers',
            'host'      => 'required|active_url|unique:servers',
            'ip'        => 'required|ip|unique:servers',
            'storage'   => 'required|integer',
            'bandwidth' => 'required|integer',
            'type'      => 'required|in:s,c,sc',
        ];

        $request->validate($rules);

        $server = Server::create($request->only(array_keys($rules)));
        return v("server.add", ['created' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function logs()
    {
        return v('server.logs', ['serverLogs' => ServerLog::with('server')->get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Server $server)
    {
        $server->update($request->all());
        return back()->with(['status' => "Server {$server->id} updated."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Server $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Server::find($id)->delete();
        return redirect("/server")->with(['status' => "Server {$id} deleted", 'type' => 'warning']);
    }
}
