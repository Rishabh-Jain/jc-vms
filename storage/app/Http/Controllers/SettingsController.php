<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    public function general ()
    {
        $settings = Setting::where('type', 'general')->select('name', 'value')->get();
        return v('settings.general', ['settings' => $settings]);
    }

    public function generalStore (Request $request)
    {
        // TODO Implement Save
        return $this->saveSettings($request);
    }

    public function profile ()
    {
        $settings = Setting::where('type', 'profile')->select('name', 'value')->get();
        return v('settings.profile', ['settings' => $settings]);
    }

    public function profileStore (Request $request)
    {
        // TODO Implement Save
        return $this->saveSettings($request);
    }

    public function advertisements ()
    {
        $settings = Setting::where('type', 'ad')->select('name', 'value')->get();
        return v('settings.advertisements', ['settings' => $settings]);
    }

    public function advertisementsStore (Request $request)
    {
        // TODO Implement Save
        return $this->saveSettings($request);
    }

    public function firewall ()
    {
        $settings = Setting::where('type', 'firewall')->select('name', 'value')->get();
        $selected = json_decode($settings->first(function ($value, $key) { return $value['name'] === 'firewall_countries'; })->value);
        $countries = json_decode(file_get_contents(\storage_path("country.json")));

        return v('settings.firewall', ['settings' => $settings, 'countries' => $countries, 'selected_countries' => $selected]);
    }

    public function firewallStore (Request $request)
    {
        // TODO Implement Save
        return $this->saveSettings($request);
    }

    public function videoPlayer ()
    {
        $settings = Setting::where('type', 'player')->select('name', 'value')->get();
        return v('settings.player', ['settings' => $settings]);
    }

    public function videoPlayerStore (Request $request)
    {
        // TODO Implement Save
        return $this->saveSettings($request);
    }

    public function saveSettings(Request $request)
    {
        $data = collect($request->all());

        $data->map(function ($value, $name ) {
            Setting::where('name', $name)->update(['value' => $value ?? '']);
        });

        return back()->with(['status' => 'Settings saved.']);
    }
}
