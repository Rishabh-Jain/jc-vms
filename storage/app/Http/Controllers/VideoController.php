<?php

namespace App\Http\Controllers;

use App\VideoLog;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

use App\Video;
use App\Server;
use App\Contracts\VideoContract;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::orderByDesc('created_at')
            ->with('server', 'user')
            ->paginate(config('settings.items_per_page', 15));
        return v('videos.list', ['videos' => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return v("videos.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'video_title' => 'required|max:180|unique:videos,title',
            'video'       => 'required|file|mimes:mp4',
            'is_raw'      => 'boolean'
        ]);

        $file = $request->file('video');

        $file->storeAs('', str_slug($request->input('video_title')) . '.' . $file->getClientOriginalExtension(), 'videos');
        Video::create([
            'title'  => $request->input("video_title"),
            'is_raw' => $request->input("is_raw", false),
        ]);

        return v("videos.add", [ 'created' => true ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return v("videos.view", ['video' => $video]);
    }

    public function logs()
    {
        return v('videos.logs', ['videoLogs' => VideoLog::with('video')->get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //NOTE Implementation not needed.
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $video->update($request->only('active'));
        return back()->with(['status' => "Video {$video->id} is updated.", 'type' => 'info']);
    }

    /**
     * Softely remove the specified resource from storage.
     *
     * @param  App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $video->delete();

        try {
            $video = Video::where('hash', $hash)->with('server')->first();

            if ($video && $video->available) {
                $host = $video->server->url;
                $uid = $video->server->uid;
                $secret = $video->server->secret;

                $request = new Client([
                    'base_uri' => $host,
                    'headers' => [
                        'JNEX-UID'      => $uid,
                        'JNEX-SECRET'   => $secret
                    ]
                ]);

                $response = $request->request('DELETE', $video->link);
                Log::info($response->getBody());
                $body = parseResponse($response->getBody());
                if ($body->status === 'error') {
                    throw new \Exception($body->error, $body->code);
                }
                $links = $body->data;
                $video->view();
                return back()->with(['status' => "Video {$video->id} deleted sucessfully.", 'type' => 'warning']);
            }
            return abort(404);

        } catch(\Exception $e) {
            Log::error($e);
            return abort(404);
        }

        return redirect("/video");
    }

    public function link(Request $request, $hash)
    {
        try {
            $video = Video::where('hash', $hash)->with('server')->first();

            if ($video && $video->available) {
                $host = $video->server->url;
                $uid = $video->server->uid;
                $secret = $video->server->secret;

                $request = new Client([
                    'base_uri' => $host,
                    'headers' => [
                        'JNEX-UID'      => $uid,
                        'JNEX-SECRET'   => $secret
                    ]
                ]);

                $response = $request->request('GET', $video->link);
                Log::info($response->getBody());

                $body = parseResponse($response->getBody());
                if ($body->status === 'error') {
                    throw new \Exception($body->error, $body->code);
                }
                $links = $body->data;
                $video->view();
                return v('videos.player', ['video' => $video, 'links' => $links]);
            }

            return abort(404);

        } catch(\Exception $e) {
            Log::error($e);
            return abort(404);
        }
    }

    public function callback(Request $request, Video $video, $hash)
    {
        if ($video->processed) {
            Log::notice("Video {$video->id} invalid callback". json_encode($request->all()));
            return response()->json(['video_id' => $video->id, 'status' => 'error']);
        }
        Log::info("Video {$video->id} callback --- ". json_encode($request->all()));
        $video->log([
            'type'      => VideoContract::type('encode'),
            'log'       => "Video {$video->id} encoded sucessfully.",
            'user_id'   => 1,
            'desc'      => $request->all(),
            'ip'        => $request->ip()
        ]);

        $video->update([
            'hash'      => $request->input('hash'),
            'processed' => true,
            'progress'  => 100
        ]);

        $video->server()->update([
            'diskusage' => $request->input('diskusage'),
            'diskfree'  => $request->input('diskfree')
        ]);

        return response()->json([
            'video_id'  => $video->id,
            'status'    =>  'OK'
        ]);
    }
}
