<?php

namespace App\Http\Controllers;

use App\VideoEncodingType;
use Illuminate\Http\Request;

class VideoEncodingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encodings = VideoEncodingType::all();
        return v('settings.encodings', ['encodings' => $encodings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //NOTE Implementation not needed.
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //NOTE Implementation not needed.
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VideoEncodingType  $encoding
     * @return \Illuminate\Http\Response
     */
    public function show($encoding)
    {
        //NOTE Implementation not needed.
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VideoEncodingType  $encoding
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoEncodingType $encoding)
    {
        //NOTE Implementation not needed.
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoEncodingType  $encoding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VideoEncodingType $encoding)
    {
        // TODO Validation
        $encoding->update($request->except('fps'));
        return back()->with(['status' => 'failed', 'id' => $encoding->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VideoEncodingType  $encoding
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoEncodingType $encoding)
    {
        //NOTE Should not be implemented.
        return back();
    }
}
