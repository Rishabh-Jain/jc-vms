<?php

namespace App\Http\Middleware;

use Closure;

class OwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        if ($type === 'account') {
            if (!$request->user()->hasRole('admin')) {
                $user = $request->route('user');
                if ($request->user()->id !== $user->id) {
                    return abort(404);
                }    
            }
        }
        return $next($request);
    }
}
