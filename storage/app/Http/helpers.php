<?php

if (! function_exists('video_path')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string  $path
     * @return string
     */
    function video_path($path = '')
    {
        return storage_path('videos'.($path ? DIRECTORY_SEPARATOR.$path : $path));
    }
}

if (! function_exists('video_exists')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string  $path
     * @return boolean
     */
    function video_exists ($name) {
        return file_exists(video_path($name));
    }
}


if (! function_exists('parseResponse')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  $response
     * @return stdClass
     */
    function parseResponse ( $response, $assoc = false ) {
        return json_decode( $response, $assoc );
    }
}

if (! function_exists('oldSettings')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  $name
     * @param  Collection $settings
     * @return string
     */
    function oldSettings ($name, $settings) {
        return old($name) ?? optional($settings->first( function ($value, $key) use ($name) { return $value['name'] === $name; } ))->value;
    }
}

if (! function_exists('v')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $view
     * @param  array   $data
     * @param  array   $mergeData
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function v($view = null, $data = [], $mergeData = [])
    {
        if ($view) {
            $view = setting('theme', 'general', '').'.'.$view;
        }

        return view($view, $data, $mergeData);
    }

}

if (! function_exists('setting')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $setting
     * @param  string   $type
     * @param  string   $default
     * @return Array
     */
    function setting($setting = null, $type = null, $default = '')
    {
        if (!$setting) {
            return '';
        }
        $setting = $type ? "{$type}.{$setting}" : $setting;
        return config("settings.{$setting}", $default);
    }

}

if (! function_exists('player')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $setting
     * @param  string   $default
     * @return Array
     */
    function player($setting = null, $default = '')
    {
        return setting($setting, 'player', $default);
    }

}

if (! function_exists('ad')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $setting
     * @param  string   $default
     * @return Array
     */
    function ad($setting = null, $default = '')
    {
        return setting($setting, 'ad', $default);
    }

}

if (! function_exists('fw')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $setting
     * @param  string   $default
     * @return Array
     */
    function fw($setting = null, $default = '')
    {
        return setting($setting, 'firewall', $default);
    }
}
