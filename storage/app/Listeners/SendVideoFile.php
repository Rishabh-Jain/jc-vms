<?php

namespace App\Listeners;

use App\Events\VideoCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Video;
use App\VideoEncodingType;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use App\Contracts\VideoContract;

class SendVideoFile
{
    /**
    * Create the event listener.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    /**
    * Handle the event.
    *
    * @param  VideoCreated  $event
    * @return void
    */
    public function handle(VideoCreated $event)
    {
        $video = $event->video;
        if (video_exists($video->fileName)) {
            $video->load('server');
            $host = $video->server->url;
            $uid = $video->server->uid;
            $secret = $video->server->secret;
            $link = "/videos";


            $http = new HttpClient([
                'base_uri' => $host,
                'headers' => [
                    'JNEX-UID'      => $uid,
                    'JNEX-SECRET'   => $secret
                ]
            ]);
            try {
                $response = $http->request('POST', $link, [
                    'multipart' => [
                        [
                            'name'  =>  'video',
                            'filename'  =>  $video->fileName,
                            'contents' => fopen($video->file, 'r')
                        ],
                        [
                            'name'  =>  'id',
                            'contents' => $video->id,
                        ],
                        [
                            'name'  =>  'encode',
                            'contents' => $video->raw ? 0 : 1,
                        ],
                        [
                            'name'  => 'encodings',
                            'contents' => VideoEncodingType::all()->toJson()
                        ],
                        [
                            'name'  => 'callback',
                            'contents' => url('/api/v1/video',['id' => $video->id , 'hash' => $video->hash])
                        ],

                    ]
                ]);

                Log::info($response->getBody());
                $body = parseResponse($response->getBody());

                if ($body->status === 'error') {
                    throw new \Exception($body->error, $body->code);
                }

                $video->log([
                    'type'  => VideoContract::type('encode'),
                    'log'   => 'started',
                    'user_id' => 1,
                    'desc' => $body,
                    'ip'    => $video->server->ip
                ]);

                unlink($video->file);
                $video->update([
                    'link'  => $video->server->url.$body->data->link
                ]);


            } catch(\Exception  $error) {
                Log::error($error);
                $video->log([
                    'type'  => VideoContract::type('encode'),
                    'log'   => 'failed',
                    'user_id' => 1,
                    'desc'  => $error->getMessage(),
                    'ip'    => $video->server->ip
                ]);
            }
        } else {
            Log::error("Video file doesn't exists.", $video->toArray());
        }


    }
}
