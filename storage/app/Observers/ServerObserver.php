<?php
namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

use App\Server;

class ServerObserver
{
    public function creating(Server $server)
    {
        $server->diskfree = $server->storage;
        $server->user_id = Auth::id() ?? 1;
        return $server;
    }

    public function created(Server $server)
    {
        $server->log([
            'user_id' => Auth::id() ?? 1,
            'ip' => $server->getAttribute('ip'),
            'type' => 'created',
            'log'  => "Server {$server->getAttribute('ip')} created.",
            'desc' => $server->only($server->getVisible())
        ]);
    }

    public function deleting(Server $server) {
        $server->log([
            'user_id' => Auth::id() ?? 1,
            'ip' => \Request::ip(),
            'type' => 'created',
            'log'  => "Server {$server->getAttribute('ip')} deleted.",
            'desc' => $server->only($server->getVisible())
        ]);
    }

}
