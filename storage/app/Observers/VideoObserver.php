<?php
namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

use App\Video;
use App\Server;
use App\Events\VideoCreated;

class VideoObserver
{
    public function creating(Video $video)
    {
        if (empty($video->server_id)) {
            $server_id = Server::where('active', true)->orderBy('diskfree')->first()->id;
        } else {
            $server_id = $video->server_id;
        }

        $video->slug = str_slug($video->title);
        $video->hash = md5($video->slug);
        $video->link = $video->slug;
        $video->server_id = $server_id;
        $video->user_id = Auth::id() ?? 1;
        $video->processed = $video->is_raw ?? true;
        return $video;
    }

    public function created(Video $video)
    {
        $video->log([
            'type' => 'created',
            'user_id' => Auth::id() ?? 1,
            'log' => 'Video Created.',
            'ip' => Request::ip(),
        ]);
        event(new VideoCreated($video));
    }
}
