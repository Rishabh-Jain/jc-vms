<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Setting;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use \Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $settings = Setting::all();
        $settings->map(function ($setting) {
            Config::set("settings.{$setting->type}.{$setting->name}", $setting->value);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path("Http/helpers.php");
    }
}
