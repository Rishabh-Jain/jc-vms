<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive("setting", function ($data) {
            try {
                $name = $data['name'] ?? $data[0];
                $collection = $data['collection'] ?? $data[1];
                $optional = $collection->first( function ($value, $key) {
                    return $value['name'] === $name;
                });
                $optional = decrypt(optional($optional)->value);
                $data = old($name, $optional);
                return "<?php echo ($data); ?>";
            } catch(Exception $e) {
                return "<?php echo $data; ?>";
            }
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
