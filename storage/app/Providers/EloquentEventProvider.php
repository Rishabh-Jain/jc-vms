<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Video;
use App\Server;
use App\Observers\VideoObserver;
use App\Observers\ServerObserver;

class EloquentEventProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Video::observe(VideoObserver::class);
        Server::observe(ServerObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
