<?php
namespace App;

use Spatie\Permission\Models\Role as eRole;

class Role extends eRole
{
    public function scopeActive ($query, $var = 1)
    {
        return $query->where('id', '!=', $var);
    }
}
