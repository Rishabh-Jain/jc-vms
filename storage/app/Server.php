<?php

namespace App;

use App\Traits\Loggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{

    use Loggable, SoftDeletes;

    protected $appends = ['url', 'secure_url'];
    protected $guarded = [];
    protected $hidden = ['secret'];
    protected $visible = ['name', 'uid', 'type', 'host', 'ip', 'storage', 'bandwidth'];
    protected $dates = ['deleted_at'];

    public function getUrlAttribute() {
        $url = parse_url($this->host);
        return 'http://' . $url['host'] . ':' . $this->port;
    }

    public function getSecureUrlAttribute() {
        $url = parse_url($this->host);
        return 'https://' . $url['host'] . ':' . $this->ssl_port;
    }

    public function setUidAttribute($id)
    {
        $this->attributes['uid'] = encrypt($id);
    }

    public function setSecretAttribute($secret)
    {
        $this->attributes['secret'] = encrypt($secret);
    }

    public function getUidAttribute($id)
    {
        try {
            return decrypt($id);
        } catch (\Exception $e) {
            return 'Error';
        }
    }

    public function getSecretAttribute($secret)
    {
        try {
            return decrypt($secret);
        } catch (\Exception $e) {
            return 'Error';
        }
    }

    public function getTypeAttribute($type)
    {
        if ($type === 's') {
            return 'S+E';
        } elseif ($type === 'c') {
            return "Client";
        } else {
            return 'NONE';
        }
    }

    public function logs()
    {
        return $this->hasMany('App\ServerLog');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
