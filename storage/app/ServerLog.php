<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\IpInfo;

class ServerLog extends Model
{
    use IpInfo;

    protected $guarded = [];


    public function setDescAttribute($desc)
    {
        try {
            $this->attributes['desc'] = json_encode($desc);
        } catch (\Exception $e) {
            $this->attributes['desc'] = ($desc);
        }
    }

    public function server()
    {
        return $this->belongsTo('App\Server');
    }
}
