<?php
namespace App\Traits;

trait Encodable {

    public function encode($item) {
        return urlencode(json_encode($this->getAttribute($item) ?? []));
    }

}
