<?php
namespace App\Traits;

use Location;

trait IpInfo
{
    public function ip_info ($attr = 'ip')
    {
        return Location::get($this->getAttribute($attr)) ?? [];
    }

    public function ip_json ($attr = 'ip')
    {
        $value = json_decode(json_encode($this->ip_info($attr)), true);
        unset($value['driver']);
        return json_encode($value);
    }
}
