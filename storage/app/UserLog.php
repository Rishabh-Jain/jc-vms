<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Encodable;
use App\Traits\IpInfo;

class UserLog extends Model
{
    use Encodable, IpInfo;

    protected $guarded = [];
    protected $casts = [
        'info' => 'array'
    ];

    public function setInfoAttribute($value) {
        $value = json_decode(json_encode($value), true);
        unset($value['driver']);
        $this->attributes['info'] = json_encode($value);
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
