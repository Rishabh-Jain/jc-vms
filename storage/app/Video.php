<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Loggable;
use App\VideoView;

class Video extends Model
{
    use Loggable;

    protected $guarded = [];
    protected $append = ['raw', 'file', 'fileName', 'pending', 'player_link', 'available'];
    protected $casts = [
        'is_raw' => 'boolean',
        'raw' => 'boolean',
        'active' => 'boolean',
        'processed' => 'boolean',
        'available' => 'boolean',
        'pending' => 'boolean',
        'status' => 'boolean',
        'views' => 'integer',
    ];

    public function view($amount = 1)
    {
        $view = VideoView::whereDate('created_at', 'today')->first();
        if ($view) {
            $video->view();
        } else {
            VideoView::create(['views' => 1]);
        }
        $this->views += $amount;
        return $this->saveOrFail();
    }

    public function getPlayerLinkAttribute() {
        return url('/video/link', $this->getAttribute('hash'));
    }

    public function getPendingAttribute()
    {
        return !$this->getAttribute('processed');
    }

    public function getRawAttribute()
    {
        return $this->getAttribute('is_raw');
    }

    public function getAvailableAttribute()
    {
        return $this->getAttribute('processed') && $this->getAttribute('active');
    }

    public function getFileNameAttribute()
    {
        return $this->getAttribute('slug') .'.mp4';
    }

    public function getFileAttribute()
    {
        return video_path($this->getAttribute('fileName'));
    }

    public function getNameAttribute()
    {
        return $this->getAttribute('title');
    }

    public function server()
    {
        return $this->belongsTo('App\Server');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function logs()
    {
    return $this->hasMany('App\VideoLog');
    }

    public function encoding()
    {
        return $this->belongsTo('App\VideoEncodingType');
    }
}
