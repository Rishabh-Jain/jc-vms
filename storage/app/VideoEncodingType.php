<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoEncodingType extends Model
{
    protected $guarded = [];

    protected $appends = ['canvas', 'fps', 'type'];

    public function getCanvasAttribute()
    {
        return $this->getAttribute('width') .'x'.$this->getAttribute('height');
    }

    public function getTypeAttribute()
    {
        return explode('_', $this->getAttribute('name'))[0];
    }

    public function getFpsAttribute()
    {
        return $this->getAttribute('quality');
    }
}
