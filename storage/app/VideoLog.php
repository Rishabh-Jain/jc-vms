<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\IpInfo;

class VideoLog extends Model
{
    use IpInfo;

    protected $guarded = [];

    public function getDescAttribute($desc)
    {
        try {
            return json_decode($desc);
        } catch (\Exception $e) {
            return $desc;
        }
    }

    public function setDescAttribute($desc)
    {
        try {
            $this->attributes['desc'] = json_encode($desc);
        } catch (\Exception $e) {
            $this->attributes['desc'] = ($desc);
        }
    }

    public function video()
    {
        return $this->belongsTo("App\Video");
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
