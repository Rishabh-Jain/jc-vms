<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoView extends Model
{
    public function view($amount = 1)
    {
        $this->views += $amount;
        return $this->saveOrFail();
    }
}
