<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'servers', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name' )->index()->unique();
			$table->string( 'host' )->index()->unique();
			$table->integer( 'ssl_port' )->index();
			$table->integer( 'port' )->index();
			$table->ipAddress( 'ip' )->index()->unique();
			$table->text( 'uid' );
			$table->text( 'secret' );
			$table->enum( 'type', [ 's', 'c', 'sc' ] );
			$table->unsignedBigInteger( 'storage' );
			$table->unsignedBigInteger( 'bandwidth' );
			$table->unsignedBigInteger( 'diskusage' )->default(0);
			$table->unsignedBigInteger( 'diskfree' );
			$table->boolean( 'active' )->default(false);
			$table->softDeletes();

            $table->unsignedInteger('user_id');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'servers' );
	}
}
