<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();

            $table->string('link')->index();
            $table->boolean('active')->default(true);
            $table->boolean('processed')->default(false);
            $table->boolean('is_raw')->default(true);
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('progress')->default(0);
            $table->char('hash', 32);
            $table->text('description')->nullable();
            $table->text('info')->nullable();
            $table->softDeletes();

            $table->unsignedInteger('server_id');
            $table->unsignedInteger('user_id');

            $table->timestamps();

            $table->foreign('server_id')->references('id')->on('servers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
