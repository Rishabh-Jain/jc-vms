/**
 * Version: 1.0
 * Author: JuicyCodes
 * Website: https://juicycodes.com
 */
$(document).ready(function() {
    $("[data-toggle=tooltip]").tooltip();
    Waves.attach('.btn-circle', ['waves-circle', 'waves-float', 'waves-light']);
    Waves.attach(".btn", ["waves-light", "waves-float"]);
    Waves.attach(".menu_link");
    Waves.init();
    $(".menu_link.active.collapsed").trigger('click');
    $(".protected").each(function() {
        $(this).popover({
            content: decodeURIComponent($(this).data("text")),
            placement: 'top',
        });
    });
    $('body').on('click', function(e) {
        $('[data-toggle="popover"]').each(function() {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});

function jc_confirm(elem = false) {
    let result = false;
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Yes!",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        if (elem) {
            result = true;
            // window.open(elem.href, (elem.target == "" ? "_self" : elem.target));
        }
    });
    return result;
}

function jc_delete(elem) {
    let result = false;
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Yes!",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        if (elem) {
            result = true;
            const form = elem.href.split('#!')[1];
            console.log(form);
            if (form) {
                console.log(form);
                $('#' + form).submit();
                return true;
            }
            // window.open(elem.href, (elem.target == "" ? "_self" : elem.target));
        }
    });
    return result;
}

function empty(mixedVar) {
    var undef
    var key
    var i
    var len
    var emptyValues = [undef, null, false, 0, '', '0']
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true
        }
    }
    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false
            }
        }
        return true
    }
    return false
}
