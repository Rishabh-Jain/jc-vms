var dot, upload, total_b, loaded_b, progress_p;
$(document).ready(function() {
    $("body").on("click", "#add_more", function() {
        $(".single_video").eq(0).removeClass("col-lg-offset-3");
        var video = $(".single_video").eq(0).clone();
        video.find("input:not(:checkbox),textarea").val("");
        video.find(".panel-footer").hide();
        video.find(".btn-remove").removeClass("hide");
        video.find(".video-subtitle").not(":first").remove();
        video.find(".sub_file").show();
        video.find(".sub_label").addClass("hide").val("");
        video.find(".sub_file").prop("name", "sub_file[" + $(".single_video").length + "][]");
        video.find(".sub_label").prop("name", "sub_label[" + $(".single_video").length + "][]");
        $(video).insertBefore("#vid_btn");
        $(".single_video").eq(0).find(".panel-footer").hide();
        $("#vid_btn").removeClass("hide");
        fix_footer();
        var icon = $(".ch").find("i");
        $(".video").prop("type", "file");
        icon.removeClass("fa-cloud-upload").addClass("fa-chain");
        $(".ch").hide();
    });

    $('body').on('click', '.panel .btn-remove', function(e) {
        e.preventDefault();
        $(this).parents('.single_video').fadeOut(300, function() {
            $(this).remove();
            if ($(".single_video").length == "1") {
                $("#vid_btn").addClass("hide");
                $(".single_video").eq(0).addClass("col-lg-offset-3");
                $(".ch").show();
                $(".single_video").eq(0).find(".panel-footer").show(function() {
                    fix_footer();
                });
            }
        });
    });

    $("#upload_video").submit(function(e) {
        e.preventDefault();
        var formdata = new FormData(this);
        if (check_video()) {
            $("#video_select").fadeOut(function() {
                $("#video_uploading").removeClass("hide").show();
                upload_video(formdata);
            });
        }
    });

    $(".cancel_upload").click(function() {
        var confirmx = confirm("Are you sure?");
        if (confirmx) {
            cancel_upload();
        }
    });

    $(".upload_again").click(function() {
        $("#video_uploaded").fadeOut(function() {
            reset_dom();
            $("#video_select").show();
            $("#upload_video")[0].reset();
        });
    });

    $("body").on('change keyup', ".video", function(e) {
        var elem = $(this).parents(".col-md-6").find(".video_title");
        if ($(this).attr("type") == "file") {
            $(elem).val(e.target.files[0].name);
        } else {
            $(elem).val(base_name(e.target.value));
        }
    });

    $("body").on('click', '#add_sub', function() {
        var sub = $(".video-subtitle").eq(0).clone();
        sub.find("input").val("");
        sub.find(".sub_file").show();
        sub.find(".sub_label").addClass("hide");
        $(sub).appendTo($(this).parents(".col-md-6").find("#subtitles"));
    });

    $("body").on("click", ".sub_file", function() {
        var elem = this;
        var parent = $(this).parent();
        $(this).fadeOut("fast", function() {
            var index = parseInt($(elem).parents(".col-md-6").find(".sub_file").index(elem) + 1);
            parent.find(".sub_label").removeClass("hide").val("SUB-" + index);
        });
    });
});

function upload_video(formdata) {
    dot = setInterval(function() {
        var dots = $("#upload_dot").text();
        if (dots == ".") {
            $("#upload_dot").text("..");
        } else if (dots == "..") {
            $("#upload_dot").text("...");
        } else {
            $("#upload_dot").text(".");
        }
    }, 800);
    if ($("#video").attr("type") == "url") {
        var data_type = false;
        var upload_url = ajax_url + "/remote_upload/";
    } else {
        var data_type = 'json';
        var upload_url = ajax_url + "/upload_video/";
    }
    upload = $.ajax({
        url: upload_url,
        type: 'POST',
        data: formdata,
        xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            var started = new Date();
            if ($("#video").attr("type") == "url") {
                xhr.onreadystatechange = function() {
                    if (xhr.readyState > 2) {
                        var data = xhr.responseText.split("~");
                        var data = data[0].split("|");
                        [total, loaded, progress] = data[data.length - 1].split("-");
                        progress_info(total, loaded, progress, started);
                    }
                };
            } else {
                xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable && e.total > 0) {
                        var loaded = e.loaded;
                        var total = e.total;
                        var progress = Math.round((e.loaded / e.total) * 100);
                        progress_info(total, loaded, progress, started);
                    }
                });
            }
            return xhr;
        },
        success: function(datas) {
            if ($("#video").attr("type") == "url") {
                var datas = datas.split("~");
                var datas = JSON.parse(datas[1]);
                var datas = {
                    0: datas
                };
            }
            $.each(datas, function(i, data) {
                if (data.status == "success") {
                    $("#video_uploading").fadeOut(function() {
                        $("#video_uploaded").removeClass("hide").show();
                    });
                    if (i > "0") {
                        $($(".embed_code").eq(0).clone()).insertAfter(".embed_code:last-child");
                        $($(".player_link").eq(0).clone().val("")).insertAfter(".player_link:last-child");
                    }
                    $(".player_link:last-child").val(data.player_link);
                    $(".embed_code:last-child").val($(".embed_code:last-child").val().replace(/src="(.*?)"/g, 'src="' + data.player_link + '"'));
                } else {
                    if (i == "0") {
                        cancel_upload(true);
                    }
                    toastr.error(data.errors.join("<br/>"));
                }
            });
        },
        error: function(e) {
            if (e.statusText != "abort") {
                cancel_upload(e);
                toastr.error("Unknown error occurred during video upload!");
            }
        },
        cache: false,
        dataType: data_type,
        contentType: false,
        processData: false,
    });
}

/**
 * Create Upload Progress Bar & Show Other Information
 * @param  {Number} total
 * @param  {Number} loaded
 * @param  {String} progress
 * @param  {Object} started
 */
function progress_info(total, loaded, progress, started) {
    if (empty(total)) {
        var total = total_b;
    } else {
        total_b = total;
    }
    if (empty(loaded)) {
        var loaded = loaded_b;
    } else {
        loaded_b = loaded;
    }
    if (empty(progress)) {
        var progress = progress_p;
    } else {
        progress_p = progress;
    }
    var seconds_elapsed = (new Date().getTime() - started.getTime()) / 1000;
    var bytes_per_second = seconds_elapsed ? loaded / seconds_elapsed : 0;
    var remaining_bytes = total - loaded;
    var seconds_remaining = seconds_elapsed ? remaining_bytes / bytes_per_second : 'Unknown';
    $("#uploaded_p").text(progress + '%');
    $(".progress-bar").width(progress + '%');
    $("#total_size").text(file_size(total));
    $("#uploaded_b").text(file_size(loaded));
    $("#time_left").text(sec_format(seconds_remaining));
    $("#up_speed").text(file_size(bytes_per_second));
}

/**
 * Check if video is selected & has a name
 * @return {Boolean}
 */
function check_video() {
    var errors = [];
    if (empty($("#video").val())) {
        errors.push("Please select a video!");
    } else {
        if ($("#video").attr("type") == "file") {
            var file = $("#video")[0].files[0];
            if (empty(file.size)) {
                errors.push("Invalid video selected!")
            } else {
                var supported = ["video/mp4", "video/3gpp", "video/x-flv", "video/webm", "video/x-msvideo", "video/x-matroska"];
                if (supported.indexOf(file.type) == "-1") {
                    //errors.push("Invalid video format!");
                }
            }
        }
    }
    if (empty(errors.length)) {
        $(".video_title").each(function() {
            if (empty($(this).val())) {
                errors.push("Please write a video title!");
            }
        });
    }
    if (errors.length > 0) {
        toastr.error(errors.join("<br/>"));
        return false;
    }
    return true;
}

/**
 * Cancel/Stop Video Uploading
 * @param  {Object} $all
 * @return {Boolean}
 */
function cancel_upload($event = false) {
    if ($event === false) {
        $("#upload_video")[0].reset();
    }
    if (!empty($event) && $event !== true) {
        console.log($event);
    }
    if (!empty(dot)) {
        clearInterval(dot);
    }
    if (!empty(upload)) {
        upload.abort();
    }
    $("#video_uploading").fadeOut(function() {
        $("#video_select").show();
        reset_dom();
    });
}

/**
 * Reset some dom element's contents
 */
function reset_dom() {
    $(".sub_file").show();
    $("#player_link").val("");
    $("#download_link").val("");
    $("#uploaded_p").text("0%");
    $("#time_left").text("Unknown");
    $(".sub_label").addClass("hide");
    $(".progress-bar").css('width', '0%');
    $(".video-subtitle").not(':first').remove();
    $(".progress-bar").attr('aria-valuenow', '0');
    $("#uploaded_b,#total_size,#up_speed").text("0");
    $(".embed_code").val($(".embed_code").val().replace(/src="(.*?)"/g, 'src=""'));
}

function fix_footer() {
    if ($('.main').height() < $('.sidebar').height()) {
        $('.main').height($('.sidebar').height());
    } else {
        $('.main').height("auto");
    }
}

/**
 * Human Readble Duration
 * @param  {Number} duration
 * @return {String}
 */
function sec_format(duration) {
    var hour = 0;
    var min = 0;
    var sec = 0;
    if (duration) {
        if (duration >= 60) {
            min = Math.floor(duration / 60);
            sec = duration % 60;
        } else {
            sec = duration;
        }
        if (min >= 60) {
            hour = Math.floor(min / 60);
            min = min - hour * 60;
        }
        sec = Math.floor(sec);
    }
    var duration = [];
    if (!empty(hour)) {
        duration.push(hour + ' hour');
    }
    if (!empty(min)) {
        duration.push(min + ' min');
    }
    duration.push(sec + ' sec');
    return duration.join(" ");
}

/**
 * Returns trailing name component of path
 * @param  {String} str
 * @return {String}
 */
function base_name(str) {
    var base = new String(str).substring(str.lastIndexOf('/') + 1);
    return decodeURIComponent(base);
}

/**
 * Return Human Readable File Size
 * @param  {Number} $byte
 * @return {String}
 */
function file_size($byte) {
    var size = filesize($byte, {
        output: "object"
    });
    return [size.value.toFixed(2), null, size.suffix].join(" ");
}