<?php
if (!defined("ROOTDIR")) {
    exit;
}
use IT\Data;

function template_header($preview = false)
{
    $components = array(
        "html"        => '<script>jwplayer.key = "' . Data::Get("jwp_key") . '";</script>',
        "stylesheets" => array("juicycodes.css"),
        "javascripts" => array("jwplayer.js", "juicycodes.js"),
    );
    return $components;
}

function template_body($source = array(), $subtitle = false, $preview = false, $slug)
{
    global $video;
    $html   = '<div id="video_player"></div>';
    $script = '
    var player = jwplayer("video_player");
    var config = {
        width: "' . Data::Get("width") . '",
        height: "' . Data::Get("height") . '",
        aspectratio: "' . Data::Get("aspect_ratio") . '",
        autostart: ' . Data::Get("autostart") . ',
        controls: ' . Data::Get("player_controls") . ',
        primary: "' . Data::Get("primary") . '",
        abouttext: "' . Data::Get("about_text") . '",
        aboutlink: "' . Data::Get("about_link") . '",
        image: "' . $preview . '",
        sources: ' . json_encode($source, JSON_UNESCAPED_SLASHES) . ',
        tracks: ' . json_encode($subtitle, JSON_UNESCAPED_SLASHES) . ',
        logo: {
            file: "' . Data::Get("logo") . '",
            link: "' . Data::Get("about_link") . '",
            position: "top-left",
        },
        captions: {
            color: "' . Data::Get("font_color") . '",
            fontSize: "' . Data::Get("font_size") . '",
            fontFamily: "' . Data::Get("font_family") . '",
            backgroundColor: "' . Data::Get("bg_color") . '",
        }
    };
    ';
    if (Data::Get("share_btn") == "on") {
        $script .= 'config.sharing = {sites: ["facebook","twitter","email","googleplus","reddit"]};';
    }
    $script .= "player.setup(config);";
    $html .= '<script type="text/javascript">' . $script . '</script>';
    return $html;
}

function template_footer($pop_ad = false, $pop_ad_code = false, $banner_ad = false, $banner_ad_code = false)
{
    $html = null;
    if ($banner_ad) {
        $html .= '
            <div class="banner" id="banner">
                <span class="close" onclick="return JuicyCodes.Close(); ">X</span>
                ' . $banner_ad_code . '
            </div>
        ';
    }
    if ($pop_ad) {
        $html .= $pop_ad_code;
    }
    return $html;
}
