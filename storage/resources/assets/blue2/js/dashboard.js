/**
 * Version: 1.0
 * Author: JuicyCodes
 * Website: https://juicycodes.com
 */
$(document).ready(function() {
    Morris.Area({
        element: 'stats-chart',
        data: views_data,
        xkey: 'date',
        resize: true,
        ykeys: ['views'],
        lineColors: ["#26A69A"],
        labels: ['Views'],
        parseTime: false,
        hideHover: 'auto'
    });
});