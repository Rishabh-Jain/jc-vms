require('./base.bootstrap');

//Chart js
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Stats",
            backgroundColor: 'rgb(67, 187, 176)',
            borderColor: 'rgb(33, 124, 115)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    },

    // Configuration options go here
    options: {}
});

// Main js
var toggleMenu = setInterval(function () {
    $('.fa-arrow-left').on('click', function () {
        $(this).removeClass('fa-arrow-left').addClass('fa-arrow-right');
        $('.leftNav').removeClass('slideInLeft').removeClass('col-sm-2').addClass('hidden');
        $('.main').removeClass('col-sm-10').addClass('col-sm-12').removeClass('mainArea');
    })

    $('.fa-arrow-right').on('click', function () {
        $(this).removeClass('fa-arrow-right').addClass('fa-arrow-left');
        $('.leftNav').addClass('slideInLeft').removeClass('hidden').addClass('col-sm-2');
        $('.main').removeClass('col-sm-12').addClass('col-sm-10').addClass('mainArea');
    })
}, 1000);
