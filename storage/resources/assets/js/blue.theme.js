$(document).ready(function() {
    $(".btn-toggle-fullwidth").on("click", function() {
        $("body").hasClass("layout-fullwidth")?$("body").removeClass("layout-fullwidth"): $("body").addClass("layout-fullwidth"), $(this).find(".lnr").toggleClass("lnr-arrow-left-circle lnr-arrow-right-circle"), $(window).innerWidth()<1025&&($("body").hasClass("offcanvas-active")?$("body").removeClass("offcanvas-active"):$("body").addClass("offcanvas-active"))
    }
    ), $(window).on("load resize", function() {
        $(this).innerWidth()<1025?$("body").addClass("layout-fullwidth"): $("body").removeClass("layout-fullwidth")
    }
    ), $(window).on("load", function() {
        $(window).innerWidth()<1025&&$(".btn-toggle-fullwidth").find(".icon-arrows").removeClass("icon-arrows-move-left").addClass("icon-arrows-move-right"), setTimeout(function() {
            $(".main").height()<$(".sidebar").height()&&$(".main").height($(".sidebar").height()-20)
        }
        , 500)
    }
    ), $('.sidebar a[data-toggle="collapse"]').on("click", function() {
        $(this).hasClass("collapsed")?$(this).addClass("active"): $(this).removeClass("active")
    }
    ), $(".sidebar-scroll").length>0&&$(".sidebar-scroll").slimScroll( {
        height: "85%", wheelStep:2
    }
    ), $(".panel .btn-remove").click(function(a) {
        a.preventDefault(), $(this).parents(".panel").fadeOut(300, function() {
            $(this).remove()
        }
        )
    }
    );
    var a=$(".panel-body");
    $(".panel .btn-toggle-collapse").clickToggle(function(b) {
        b.preventDefault(), $(this).parents(".panel").find(".slimScrollDiv").length>0&&(a=$(".slimScrollDiv")), $(this).parents(".panel").find(a).slideUp(300), $(this).find("i.lnr-chevron-up").toggleClass("lnr-chevron-down")
    }
    , function(b) {
        b.preventDefault(), $(this).parents(".panel").find(".slimScrollDiv").length>0&&(a=$(".slimScrollDiv")), $(this).parents(".panel").find(a).slideDown(300), $(this).find("i.lnr-chevron-up").toggleClass("lnr-chevron-down")
    }
    ), $(".panel-scrolling").length>0&&$(".panel-scrolling .panel-body").slimScroll( {
        height: "430px", wheelStep:2
    }
    ), $("#panel-scrolling-demo").length>0&&$("#panel-scrolling-demo .panel-body").slimScroll( {
        height: "175px", wheelStep:2
    }
    )
}

),
$.fn.clickToggle=function(a, b) {
    return this.each(function() {
        var c=!1;
        $(this).bind("click", function() {
            return c?(c=!1, b.apply(this, arguments)): (c=!0, a.apply(this, arguments))
        }
        )
    }
    )
}

;
