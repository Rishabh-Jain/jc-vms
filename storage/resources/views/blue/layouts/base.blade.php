<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>

<body>
    <div class="container-fluid">
        <div class="row">

            <!-- Left Navigation -->

            <div class="col-sm-2 leftNav animated slideInLeft">
                <div class="logo">
                    {{ config('app.name') }}
                </div>
                <ul class="navBar">
                    <li class="active"><a href="{{ url( '/' ) }}"><i class="fas fa-tachometer-alt" aria-hidden="true"></i> <span>Dashboard</span></a></li>
                    <li><a href="{{ url('video/create') }}"><i class="fas fa-cloud-upload-alt" aria-hidden="true"></i> <span>Upload Video</span></a></li>
                    <li><a href="{{ url('video') }}"><i class="fas fa-bars" aria-hidden="true"></i> <span>Manage Videos</span></a></li>
                    <li data-toggle="collapse" data-target="#settings"><a href="javascript:void()"><i class="fas fa-sliders-h" aria-hidden="true"></i> <span>Settings</span> <i class="fa fa-chevron-right pull-right" aria-hidden="true"></i></a></li>
                    <div id="settings" class="collapse">
                        <ul class="leftDropDown">
                            <li><a href="{{ url('settings/general') }}">General Settings</a></li>
                            <li><a href="{{ url('settings/profile') }}">Profile Settings</a></li>
                            <li><a href="{{ url( "video/encoding" ) }}">Misc Settings</a></li>
                            <li><a href="{{ url('settings/video-player') }}">Player Settings</a></li>
                            <li><a href="{{ url('settings/advertisements') }}">Advertise Settings</a></li>
                            <li><a href="{{ url( "settings/video-player" ) }}">Firewall Settings</a></li>
                        </ul>
                    </div>
                    <li data-toggle="collapse" data-target="#servers"><a href="javascript:void()"><i class="fa fa-server" aria-hidden="true"></i> <span>Servers</span> <i class="fa fa-chevron-right pull-right" aria-hidden="true"></i></a></li>
                    <div id="servers" class="collapse">
                        <ul class="leftDropDown">
                            <li><a href="{{ url( "server/create" ) }}">New Server</a></li>
                            <li><a href="{{ url( "server/logs" ) }}">Servers Logs</a></li>
                            <li><a href="{{ url( "server" ) }}">Manage Servers</a></li>
                        </ul>
                    </div>
                    <li data-toggle="collapse" data-target="#users"><a href="javascript:void()"><i class="fa fa-users" aria-hidden="true"></i> <span>Users</span> <i class="fa fa-chevron-right pull-right" aria-hidden="true"></i></a></li>
                    <div id="users" class="collapse">
                        <ul class="leftDropDown">
                            <li><a href="{{ url( "users/create" ) }}">New User</a></li>
                            <li><a href="{{ url( "users/logs" ) }}">Login Log</a></li>
                            <li><a href="{{ url( "users/list" ) }}">Manage Users</a></li>
                        </ul>
                    </div>

                </ul>
                <a href="#" class="leftNavFooter">
                    <i class="fa fa-life-ring"></i>
                    <span>SUPPORT CENTER</span>
                </a>
            </div>

            <!-- ************ -->

            <div class="col-sm-10 main mainArea">

                <!-- Header -->

                <div class="row header">
                    <i class="fa fa-arrow-left collapseNav" aria-hidden="true"></i>
                    <ul class="profileMenu pull-right">
                        <li>
                            <a href="#"><i class="fa fa-code" aria-hidden="true"></i> <span>{{ Auth::user()->name }}</span>
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown">
                                <li><a href="{{ url('/user', Auth::user()->id) }}"><i class="fa fa-cog" aria-hidden="true"></i> <span>Edit Profile</span></a></li>
                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> <span>Logout</span></a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>



                <!-- ************ -->

                <div class="row changeContent">
                    @yield('content')
                </div>
                <footer>
                    <div class="container-fluid">
                        <p>
                            Designed &amp; Crafted With
                            <i class="fa fa-heart heart"></i> by XYZ</a>.
                        </p>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/base.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.1/js/all.js"></script>
</body>

</html>
