<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/blue.css') }}" rel="stylesheet">
    <script src="{{ asset('js/blue.js') }}"></script>

</head>

</html>
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box">
                <div class="logo text-center">
                    <img src="http://falcon.streamgo.me/assets/img/juicycodes.png" alt="JUICYCODES.COM"></img>
                </div>
                <div class="content">
                    <form method="post" class="form-auth-small">
                        <div class="form-group">
                            <label for="jc_email" class="control-label">Email:</label>
                            <input id="jc_email" name="jc_email" type="email" class="form-control" placeholder="Enter your email address"></input>
                        </div>
                        <div class="form-group">
                            <label for="jc_password" class="control-label">Password:</label>
                            <input id="jc_password" name="password" type="password" class="form-control" placeholder="Enter your password"></input>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-lg btn-block">LOGIN</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="juicycodes">
                Made with <i class="fa fa-heart heart"></i> by <a href="https://juicycodes.com">JUICYCODES.COM</a>
            </div>
        </div>
    </div>
</div>

</body>

</html>
