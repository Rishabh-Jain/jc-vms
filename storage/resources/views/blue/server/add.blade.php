@extends('blue.layouts.base') @section('content')

<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Add New Server</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-10">


            @if(!empty($created))
            <div class="alert alert-success">The server has been created.</div>
            @endif
            <form class="m-3" method="post" action="{{ action('ServerController@store') }}">
                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Name : </strong>
                    </label>
                    <input class="form-control" name="name" placeholder="Server Name" value="{{ old('name') }}" />
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>ID : </strong>
                    </label>
                    <input class="form-control" name="uid" placeholder="Server ID" value="{{ old('uid') }}" />
                    @if ($errors->has('uid'))
                    <span class="help-block">
                        <strong>{{ $errors->first('uid') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Access Key : </strong>
                    </label>
                    <input type="password" class="form-control" name="secret" value="{{ old('secret') }}" placeholder="Server Key" />
                    @if ($errors->has('secret'))
                    <span class="help-block">
                        <strong>{{ $errors->first('secret') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Domain : </strong>
                    </label>
                    <input class="form-control" name="host" placeholder="Server Domain" value="{{ old('host') }}" />
                    @if ($errors->has('host'))
                    <span class="help-block">
                        <strong>{{ $errors->first('host') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">

                    <label class="control-label text-right">
                        <strong>Server Ip : </strong>
                    </label>
                    <input class="form-control" name="ip" placeholder="Server ip" value="{{ old('ip') }}" />
                    @if ($errors->has('ip'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ip') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Storage : </strong>
                    </label>
                    <div class="input-group">
                        <input type="number" class="form-control" name="storage" placeholder="Storage" value="{{ old('storage') }}" />
                        <span class="input-group-addon">MB</span>
                    </div>
                    @if ($errors->has('storage'))
                    <span class="help-block">
                        <strong>{{ $errors->first('storage') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Bandwidth : </strong>
                    </label>
                    <div class="input-group">
                        <input type="number" class="form-control" name="bandwidth" placeholder="Bandwidth" value="{{ old('bandwidth') }}" />
                        <span class="input-group-addon">MB</span>
                    </div>
                    @if ($errors->has('bandwidth'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bandwidth') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Public URL : </strong>
                    </label>
                    <input type="text" class="form-control" name="uri" placeholder="Public URL" />
                    @if ($errors->has('uri'))
                    <span class="help-block">
                        <strong>{{ $errors->first('uri') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label text-sm-right">
                        <strong>Type : </strong>
                    </label>
                    <div class="input-group">
                        <select name="type" class="form-control custom-select">
                            <option value="s">Storage + Encoding</option>
                        </select>
                        @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-sm-6 col-sm-offset-6">
                    {{ csrf_field() }}
                    <button type="submit" class="btn pull-right btnUpload">Add Server</button>
                </div>

        </div>
        </form>
    </div>
</div>

</div>

@endsection
