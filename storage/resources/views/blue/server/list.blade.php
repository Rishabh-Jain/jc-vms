@extends('blue.layouts.base')
@section('content')
                <div class="col-sm-12 manageVideo">
                    <div class="panelHead">
                        <span>Manage Videos</span> <a data-toggle="collapse" data-target="#toggleVideo" href="javascript:void"><i class="pull-right fa fa-angle-up"></i></a>
                    </div>

                    <div class="panelBody collapse in" id="toggleVideo">
                        <div class="table-responsive">
                        <table class="table table-striped no-margins table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#id</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">IP Address</th>
                                    <th scope="col-sm-2">Public URL</th>
                                    <th scope="col">Server ID</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Added</th>
                                    <th scope="col">Free</th>
                                    <th scope="col">Used</th>
                                    <th scope="col">Status</th>
                                    <th scope="col-sm-2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($servers as $server)
                                    <tr>
                                        <td>{{ $server->id }}</td>
                                        <td>{{ $server->name }}</td>
                                        <td>{{ $server->ip }}</td>
                                        <td>{{ $server->host }}</td>
                                        <td><button type="button" class="btn btn-sm btn-danger px-1 py-0" data-toggle="popover" title="Server ID" data-content="{{ $server->uid }}">Show</button></td>
                                        <td>{{ $server->type }}</td>
                                        <td>{{ $server->created_at->toDateString() }}</td>
                                        <td>{{ $server->diskfree }}MB</td>
                                        <td>{{ $server->diskusage }}MB</td>
                                        <td class="text-center">
                                            <span class="badge badge-{{ $server->active ? 'success' : 'danger' }}">
                                                {{ $server->active ? 'Active' : 'Disabled' }}
                                            </span>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-sm mr-1 px-1 py-0" style="float:left;"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <form action="{{ url('/server', ['id' => $server->id]) }}" class="" style="float:left;" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-sm mr-1 px-1 py-0"><i class="far fa-trash-alt"></i></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection
