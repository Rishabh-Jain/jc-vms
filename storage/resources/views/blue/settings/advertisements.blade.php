@extends('blue.layouts.base') @section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Advertisement Settings</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-12">
            @if (session('status'))
            <div class="alert alert-success my-2">
                {{ session('status') }}
            </div>
            @endif
            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action('SettingsController@advertisementsStore') }}">

                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Popup ads: </strong>
                    </label>
                    <div class="col">
                        <select name="popup_ad" class="form-control">
                            <option value="n" {{ oldSettings('popup_ad', $settings) == false ? 'selected' : '' }}>Disabled</option>
                            <option value="y" {{ oldSettings('popup_ad', $settings) == true ? 'selected' : '' }}>Enabled</option>
                        </select>
                        @if ($errors->has('popup_ad'))
                        <span class="help-block">
                            <strong>{{ $errors->first('popup_ad') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Popup ads code: </strong>
                    </label>
                    <div class="col">
                        <textarea class="form-control" name="popup_ad_code" placeholder="Popup Adds">{{ oldSettings('banner_ad_code', $settings) }}</textarea>
                        @if ($errors->has('popup_ad_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('popup_ad_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Banner ads: </strong>
                    </label>
                    <div class="col">
                        <select name="banner_ad" class="form-control">
                            <option value="n" {{ oldSettings('banner_ad', $settings) == false ? 'selected' : '' }}>Disabled</option>
                            <option value="y" {{ oldSettings('banner_ad', $settings) == true ? 'selected' : '' }}>Enabled</option>
                        </select>
                        @if ($errors->has('banner_ad'))
                        <span class="help-block">
                            <strong>{{ $errors->first('banner_ad') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Banner ads code: </strong>
                    </label>
                    <div class="col">
                        <textarea class="form-control" name="banner_ad_code" placeholder="Popup Adds">{{ oldSettings('banner_ad_code', $settings) }}</textarea>
                        @if ($errors->has('banner_ad_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('banner_ad_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 ml-auto">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
