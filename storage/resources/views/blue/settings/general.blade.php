@extends('blue.layouts.base') @section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>General Settings</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-10">

            @if (session('status'))
            <div class="alert alert-success my-2">
                {{ session('status') }}
            </div>
            @endif
            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action('SettingsController@generalStore') }}">
                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Website URL: </strong>
                    </label>
                        <input class="form-control" name="website_url" placeholder="Website URL" value="{{ oldSettings('website_url', $settings) }}"
                        />
                        @if ($errors->has('website_url'))
                        <span class="help-block">
                            <strong>{{ $errors->first('website_url') }}</strong>
                        </span>
                        @endif
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Theme: </strong>
                    </label>
                        <select name="theme" class="form-control">
                            <option value="default"{{ oldSettings('theme', $settings) === 'default' ? ' selected' : ''}}>Default grey</option>
                            <option value="blue" {{ oldSettings('theme', $settings) === 'blue' ? ' selected': '' }}>Blue</option>
                        </select>
                        @if ($errors->has('theme'))
                        <span class="help-block">
                            <strong>{{ $errors->first('theme') }}</strong>
                        </span>
                        @endif
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Admin URL: </strong>
                    </label>
                        <input class="form-control" name="admin_url" placeholder="Admin URL" value="{{ oldSettings('admin_url', $settings) }}" />
                        @if ($errors->has('admin_url'))
                        <span class="help-block">
                            <strong>{{ $errors->first('admin_url') }}</strong>
                        </span>
                        @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Embed Slug</strong>
                    </label>
                        <input class="form-control" name="embed_slug" placeholder="Embed Slug" value="{{ oldSettings('embed_slug', $settings) }}"
                        />
                        @if ($errors->has('embed_slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('embed_slug') }}</strong>
                        </span>
                        @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">
                        <strong>Website Title: </strong>
                    </label>
                        <input class="form-control" name="website_title" placeholder="Website Title" value="{{ oldSettings('website_title', $settings) }}"
                        />
                        @if ($errors->has('website_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('website_title') }}</strong>
                        </span>
                        @endif
                </div>
                <div class="clearfix"></div>

                <div class="form-group pull-right">
                    <div class="ml-auto">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btnUpload">Save Settings</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
