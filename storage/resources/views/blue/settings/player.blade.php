@extends('blue.layouts.base') @section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Player Settings</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-12">
                @if (session('status'))
                <div class="alert alert-success my-2">
                    {{ session('status') }}
                </div>
                @endif
                <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action('SettingsController@videoPlayerStore') }}">
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Player Type</strong>
                        </label>
                        <div class="col-3">
                            <select name="video_player" class="form-control">
                                <option value="videojs">Video JS</option>
                                <option value="jwplayer">JW Player</option>
                            </select>
                            @if ($errors->has('video_player'))
                            <span class="help-block">
                                <strong>{{ $errors->first('video_player') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>JW Player key</strong>
                        </label>
                        <div class="col">
                            <input class="form-control" name="jwplayer_licence_key" placeholder="JWPLAYER Licence" value="{{ oldSettings('jwplayer_licence_key', $settings) }}"
                            /> @if ($errors->has('jwplayer_licence_key'))
                            <span class="help-block">
                                <strong>{{ $errors->first('jwplayer_licence_key') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>About Text: </strong>
                        </label>
                        <div class="col">
                            <input class="form-control" name="jwplayer_about_text" placeholder="About Text" value="{{ oldSettings('jwplayer_about_text', $settings) }}"
                            /> @if ($errors->has('jwplayer_about_text'))
                            <span class="help-block">
                                <strong>{{ $errors->first('jwplayer_about_text') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>About Link: </strong>
                        </label>
                        <div class="col">
                            <input class="form-control" name="jwplayer_about_link" placeholder="About Link" value="{{ oldSettings('jwplayer_about_link', $settings) }}"
                            /> @if ($errors->has('jwplayer_about_link'))
                            <span class="help-block">
                                <strong>{{ $errors->first('jwplayer_about_link') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Player Width: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="player_width" placeholder="Player Width" value="{{ oldSettings('player_width', $settings) }}"
                            /> @if ($errors->has('player_width'))
                            <span class="help-block">
                                <strong>{{ $errors->first('player_width') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Player Height: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="player_height" placeholder="Player Height" value="{{ oldSettings('player_height', $settings) }}"
                            /> @if ($errors->has('player_height'))
                            <span class="help-block">
                                <strong>{{ $errors->first('player_height') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Subtitle font size: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="subtitle_font_size" placeholder="Font size" value="{{ oldSettings('subtitle_font_size', $settings) }}"
                            /> @if ($errors->has('subtitle_font_size'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtitle_font_size') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Subtitle Font Color: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="subtitle_font_color" placeholder="Font color" value="{{ oldSettings('subtitle_font_color', $settings) }}"
                            /> @if ($errors->has('subtitle_font_color'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtitle_font_color') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Subtitle Font Family: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="subtitle_font_family" placeholder="Font Family" value="{{ oldSettings('subtitle_font_family', $settings) }}"
                            /> @if ($errors->has('subtitle_font_family'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtitle_font_family') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Sub Background: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="subtitle_background_color" placeholder="Background color" value="{{ oldSettings('subtitle_background_color', $settings) }}"
                            /> @if ($errors->has('subtitle_background_color'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtitle_background_color') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Aspact Ratio: </strong>
                        </label>
                        <div class="col">
                            <input type="text" class="form-control" name="player_width" placeholder="Aspact ratio" value="{{ oldSettings('aspact_ratio', $settings) }}"
                            />
                            @if ($errors->has('aspact_ratio'))
                            <span class="help-block">
                                <strong>{{ $errors->first('aspact_ratio') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Player Controls</strong>
                        </label>
                        <div class="col">
                            <select name="player_controls" class="form-control">
                                <option value="1">Visible</option>
                                <option value="0">Hidden</option>
                            </select>
                            @if ($errors->has('player_controls'))
                            <span class="help-block">
                                <strong>{{ $errors->first('player_controls') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Video Autostrat</strong>
                        </label>
                        <div class="col">
                            <select name="video_autostart" class="form-control">
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                            @if ($errors->has('video_autostart'))
                            <span class="help-block">
                                <strong>{{ $errors->first('video_autostart') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-sm-3">
                        <label class="control-label">
                            <strong>Share button</strong>
                        </label>
                        <div class="col">
                            <select name="jwplayer_share_button" class="form-control">
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                            @if ($errors->has('jwplayer_share_button'))
                            <span class="help-block">
                                <strong>{{ $errors->first('jwplayer_share_button') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-sm-3 pull-right">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection
