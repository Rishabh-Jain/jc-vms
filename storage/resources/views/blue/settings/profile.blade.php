@extends('blue.layouts.base') @section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Profile Settings</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-12">
            @if (session('status'))
            <div class="alert alert-success my-2">
                {{ session('status') }}
            </div>
            @endif
            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action('SettingsController@profileStore') }}">
                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Default Preview Link: </strong>
                    </label>
                    <div class="col">
                        <input class="form-control" name="default_priview_link" placeholder="Default Preview Link" value="{{ oldSettings('default_priview_link', $settings) }}"
                        /> @if ($errors->has('default_priview_link'))
                        <span class="help-block">
                            <strong>{{ $errors->first('default_priview_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Default Video Link: </strong>
                    </label>
                    <div class="col">
                        <input class="form-control" name="default_video_link" placeholder="Default Video Link" value="{{ oldSettings('default_video_link', $settings) }}"
                        /> @if ($errors->has('default_video_link'))
                        <span class="help-block">
                            <strong>{{ $errors->first('default_video_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Items Per Page: </strong>
                    </label>
                    <div class="col">
                        <input type="number" class="form-control" name="items_per_page" placeholder="Items Per Page" value="{{ oldSettings('items_per_page', $settings) }}"
                        /> @if ($errors->has('items_per_page'))
                        <span class="help-block">
                            <strong>{{ $errors->first('items_per_page') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label class="col-form-label col-3">
                        <strong>Video Sub Title: </strong>
                    </label>
                    <div class="col">
                        <input class="form-control" name="video_sub_titles" placeholder="Video Sub Title" value="{{ oldSettings('video_sub_titles', $settings) }}"
                        /> @if ($errors->has('video_sub_titles'))
                        <span class="help-block">
                            <strong>{{ $errors->first('video_sub_titles') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 ml-auto">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
