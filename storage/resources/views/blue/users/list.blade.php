@extends('blue.layouts.base')
@section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Users list</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-12">
                        <table class="table table-striped no-margins table-hover">
                            <thead class="thead">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th scope="col">Videos</th>
                                <th scope="col">Active</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td><a href="{{ url("user", $user->username) }}">{{ $user->username }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->videos_count }}</td>
                                    <td>{{ $user->active }}</td>
                                    <td>
                                        @if($user->username !== 'system')
                                            <a class="btn btn-outline-primary btn-sm" href="{{ url("user", $user->username)  }}">
                                                <span class="glyphicon glyphicon-pencil"></span>Edit
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
