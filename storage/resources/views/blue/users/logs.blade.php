@extends('blue.layouts.base')
@section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Users Log</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-12">
                        <table class="table table-striped no-margins table-hover">
                            <thead class="thead">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Username</th>
                                <th scope="col">Type</th>
                                <th scope="col">Message</th>
                                <th scope="col">IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($userLogs as $userLog)
                                <tr>
                                    <td>{{ $userLog->id }}</td>
                                    <td>{{ $userLog->user->name }}</td>
                                    <td><a href="{{ url("user", $userLog->user->username) }}">{{ $userLog->user->username }}</a></td>
                                    <td>{{ $userLog->type }}</td>
                                    <td>{{ $userLog->log }}</td>
                                    <td>{{ $userLog->ip }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection
