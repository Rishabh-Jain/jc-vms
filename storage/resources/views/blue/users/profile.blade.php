@extends('blue.layouts.base')
@section('content')
<div class="col-sm-12 uploadVideo">
    <div class="panelHead">
        <span>Profile</span>
        <a data-toggle="collapse" data-target="#toggleNewServer" href="javascript:void">
            <i class="pull-right fa fa-angle-up"></i>
        </a>
    </div>

    <div class="panelBody collapse in" id="toggleNewServer">
        <div class="newServer col-sm-10">
            <form method="POST" action="{{ url('user', $user->username) }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-sm-12">
                    <label for="name" class="control-label">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ?? $user->name }}" required> @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-sm-12">
                    <label for="email" class="control-label">E-Mail Address</label>

                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') ?? $user->email }}" required> @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-sm-12">
                    <label for="password" class="control-label">New Password</label>

                    <input id="password" type="password" class="form-control" name="password"> @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <hr />
                <div class="form-group{{ $errors->has('password_old') ? ' has-error' : '' }} col-sm-12">
                    <label for="password" class="control-label">Current Password</label>

                    <input id="password_old" type="password" class="form-control" name="password_old" required> @if ($errors->has('password_old'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_old') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group row">
                    <div class="col-sm-4 pull-right">
                        <button type="submit" class="btn btn-dark btn-block">
                            Login
                        </button>

                        {{--
                        <a class="btn btn-link text-white" href="{{ route('password.request') }}">--}} {{--Forgot Your Password?--}} {{--
                        </a>--}}
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
