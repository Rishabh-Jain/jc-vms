@extends('blue.layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header bg-dark text-light">
                    Edit Encoding
                </div>
                <div class="card-block">
                    <form class="m-3" method="post" action="{{ action("VideoEncodingController@store") }}">
                        {{ method_field('PATCH') }}
                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">
                                <strong>Name : </strong>
                            </label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{ $encoding->name }}" disabled/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">
                                <strong>Width : </strong>
                            </label>
                            <div class="input-group col-sm-4">
                                <input type="number" class="form-control" name="width" placeholder="Width" value="{{ old('width') ?? $encoding->width }}"/>
                                <span class="input-group-addon">PX</span>
                                @if ($errors->has('width'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-form-label col-sm-2 text-sm-right">
                                <strong>Height : </strong>
                            </label>
                            <div class="input-group col-sm-4">
                                <input type="number" class="form-control" name="height" placeholder="Height" value="{{ old('height') ?? $encoding->height }}"/>
                                <span class="input-group-addon">PX</span>
                                @if ($errors->has('height'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">
                                <strong>Framerate : </strong>
                            </label>
                            <div class="input-group col-sm-4">
                                <input type="number" class="form-control" name="fps" placeholder="Framerate" value="{{ old('fps') ?? $encoding->fps }}"/>
                                @if ($errors->has('fps'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fps') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-form-label col-sm-2 text-sm-right">
                                <strong>Audio Bitrate : </strong>
                            </label>
                            <div class="input-group col-sm-4">
                                <select name="bitrate" class="form-control custom-select">
                                    <option value="64">64</option>
                                    <option value="96">96</option>
                                    <option value="128">128</option>
                                    <option value="192">192</option>
                                </select>
                                @if ($errors->has('bitrate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bitrate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2 ml-auto">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-sm btn-primary btn-block disabled">Edit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
