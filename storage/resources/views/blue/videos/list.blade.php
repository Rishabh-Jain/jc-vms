@extends('blue.layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header bg-dark text-light">
                    Videos
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-hover table-bordered">
                            <thead class="bg-default">
                                <tr class="text-center">
                                    <th scope="col-sm-2">Title</th>
                                    <th scope="col-sm-2">Player Link</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Is Raw</th>
                                    <th scope="col-sm-2">Views</th>
                                    <th scope="col">Added</th>
                                    <th scope="col">Server</th>
                                    <th scope="col-sm-2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($videos as $video)
                                    <tr>
                                        <td class="" title="{{ $video->slug }}">{{ $video->title }}</td>
                                        <td class="text-center ">
                                            <a class="btn btn-sm btn-success px-1 py-0" target="_blank" href="{{ url('video/link', $video->hash) }}">Link</a>
                                        </td>
                                        <td class="text-center text-light">
                                            @if ($video->processed)
                                            <span class="badge bg-{{ $video->active ? 'success' : 'info' }} p-1">
                                                {{ $video->active ? 'Active' : 'Disabled' }}
                                            </span>
                                            @else
                                                <span class="bg-info p-1 m-0 badge">Queued</span>
                                            @endif
                                        </td>
                                        <td class="text-center text-light">
                                            @if ($video->processed)
                                            <span class="badge bg-{{ !$video->raw ? 'success' : 'info' }} p-1">
                                                {{ $video->raw ? 'Raw' : 'Encoded' }}
                                            </span>
                                            @else
                                                <span class="bg-info p-1 m-0 badge">Queued</span>
                                            @endif
                                        </td>
                                        <td class="text-center "><span class="bg-info badge p-1 text-light">{{ $video->views }}</span></td>
                                        <td class="text-center ">{{ $video->user->name}}</td>
                                        <td class="text-center ">{{ $video->server->name }}</td>
                                        <td class="">
                                            <a class="btn btn-primary btn-sm mr-1 px-1 py-0" href="{{ url("video", $video->id) }}" style="float:left;"><i class="fas fa-bars" aria-hidden="true"></i></a>
                                            <a class="btn btn-primary btn-sm mr-1 px-1 py-0" style="float:left;"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <form action="{{ url('/video', ['id' => $video->id]) }}" class="" style="float:left;" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-sm mr-1 px-1 py-0"><i class="far fa-trash-alt"></i></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
