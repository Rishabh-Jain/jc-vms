<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link href="//vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
    <!-- If you'd like to support IE8 -->
    <script src="//vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
<body>
<video id="my-video" class="video-js" controls preload="auto" width="{{ $video->width }}" height="{{ $video->height }}" poster="{{ $video->server->url . $links->image }}" data-setup="{}" style="margin: auto">
    @foreach($links->video as $qty => $link)
        <source src="{{ $video->server->url . $link->play }}" type='video/mp4' label="{{ $qty }}" res="{{ $qty }}">
    @endforeach
    <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to a web browser that
        <a href="//videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
</video>

<script src="//vjs.zencdn.net/6.4.0/video.js"></script>
</body>
</html>

