@extends('blue2.layouts.noSide')

@section('content')
    <div class="auth-box">
                <div class="logo text-center">
                    <img src="{{ asset('/blue2/img/juicycodes.png') }}" alt="JUICYCODES.COM"></img>
                </div>
                <div class="content">
                    @if($errors->count())
                    <div class="alert alert-danger text-left" role="alert">Invalid User Credentials!</div>
                    @endif
                    <form method="post" class="form-auth-small">
                        <div class="form-group">
                            <label for="jc_email" class="control-label">Email:</label>
                            <input id="jc_email" name="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="Enter your email address"></input>
                        </div>
                        <div class="form-group">
                            <label for="jc_password" class="control-label">Password:</label>
                            <input id="jc_password" name="password" type="password" class="form-control" placeholder="Enter your password"></input>
                        </div>
                        <div class="form-group">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success btn-lg btn-block">LOGIN</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection
