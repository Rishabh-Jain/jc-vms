@extends('blue2.layouts.base')

@section('content')

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add New User</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse">
                                    <i class="lnr lnr-chevron-up"></i>
                                </button>
                            </div>
                        </div>
                        <form method="post" action="http://falcon.streamgo.me/actions/">
                            <input name="action" type="hidden" class="hide" value="add_user"></input>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-4">
                                        <div class="form-group">
                                            <label for="jc_name" class="control-label">User Full Name:</label>
                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Insert User Full Name"></i>
                                            <input id="jc_name" name="name" value="{{ old('name') }}" type="text" class="form-control" placeholder="Insert User Full Name"></input>
                                        </div>
                                        <div class="form-group">
                                            <label for="username" class="control-label">Username:</label>
                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Insert Username"></i>
                                            <input id="username" name="username" value="{{ old('username') }}" type="text" class="form-control" placeholder="Insert Username"></input>
                                        </div>
                                        <div class="form-group">
                                            <label for="jc_email" class="control-label">User Email:</label>
                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Insert User Email"></i>
                                            <input id="jc_email" name="email" value="{{ old('email') }}" type="text" class="form-control" placeholder="Insert User Email"></input>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="jc_pass" class="control-label">User Password:</label>
                                                    <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Insert User Password"></i>
                                                    <input id="jc_pass" name="password" value="{{ old('password') }}" type="password" class="form-control" placeholder="Insert User Password"></input>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="jc_role" class="control-label">User Role:</label>
                                                    <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Select User Role"></i>
                                                    <select class="form-control" id="jc_role" name="role">
                                                        <option value="0" selected="selected">-- Select A Role --</option>
                                                        @foreach(\App\Role::active()->get() as $role)
                                                            <option value="{{ $role->name }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-info">ADD USER</button>
                            </div>
                        </form>
                    </div>
@endsection
<div class="form-group">
    <label for="password" class="col-md-4 control-label">Roles</label>
    <div class="col-md-12">
        <select name="role" class="form-control">
            @foreach(\App\Role::active()->get() as $role)
                <option value="{{ $role->name }}">{{ $role->name }}</option>
            @endforeach
        </select>
    </div>
</div>
