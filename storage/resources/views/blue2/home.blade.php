@extends('blue2.layouts.base')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">LAST 15 DAYS STATISTICS</h3>
            </div>
            <div class="panel-body no-paddings">
                <div id="stats-chart" class=""></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-body text-center">
                <div class="big-number">11,209</div>
                <div class="number-header">Total Video Uploaded</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-body text-center">
                <div class="big-number">02</div>
                <div class="number-header">Storage Server Available</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-body text-center">
                <div class="big-number">01</div>
                <div class="number-header">Encoding Server Available</div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('footer_scripts')

<script src="{{ asset('/blue2/js/dashboard.js') }}"></script>

<script type="text/javascript">

    var views_data = {!! $views !!};

</script>
@endpush
