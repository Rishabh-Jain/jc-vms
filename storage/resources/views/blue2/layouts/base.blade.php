<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/blue2/css/bootstrap.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/icon-sets.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/waves.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/sweetalert.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/main.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/app.css?1513618896') }}"></link>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></link>
    <script src="{{ asset('/blue2/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/slimscroll/slimscroll.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/waves/waves.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/theme.min.js') }}"></script>
    @stack('scripts')
    <script type="text/javascript">
        var ajax_url = "{{ url('/ajax') }}";

    </script>

</head>

<div id="wrapper">
    <div class="sidebar">
        <div class="brand">
            <a href="{{ url('/') }}">
                <img src="{{ asset('/blue2/img/juicycodes.png') }}" alt="JUICYCODES.COM" class="img-responsive logo"></img>
            </a>
        </div>
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <li>
                        <a href="{{ url('/') }}" class="menu_link active">
                            <i class="fa fa-tachometer"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/video/create') }}" class="menu_link">
                            <i class="fa fa-cloud-upload"></i>
                            <span>Upload Video</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/video') }}" class="menu_link">
                            <i class="fa fa-bars"></i>
                            <span>Manage Videos</span>
                        </a>
                    </li>
                    <li>
                        <a href="#servers" data-toggle="collapse" class="menu_link collapsed">
                            <i class="fa fa-server"></i>
                            <span>Servers</span>
                            <i class="icon-submenu lnr lnr-chevron-left"></i>
                        </a>
                        <div id="servers" class="collapse">
                            <ul class="nav">
                                <li>
                                    <a href="{{ url('/server/create') }}">New Server</a>
                                </li>
                                <li>
                                    <a href="{{ url('/server') }}">Manage Servers</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#users" data-toggle="collapse" class="menu_link collapsed">
                            <i class="fa fa-users"></i>
                            <span>Users</span>
                            <i class="icon-submenu lnr lnr-chevron-left"></i>
                        </a>
                        <div id="users" class="collapse">
                            <ul class="nav">
                                <li>
                                    <a href="{{ url('/register') }}">New User</a>
                                </li>
                                <li>
                                    <a href="{{ url('/users/logs') }}">Login Log</a>
                                </li>
                                <li>
                                    <a href="{{ url('/users/list') }}">Manage Users</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#settings" data-toggle="collapse" class="menu_link collapsed">
                            <i class="fa fa-sliders"></i>
                            <span>Settings</span>
                            <i class="icon-submenu lnr lnr-chevron-left"></i>
                        </a>
                        <div id="settings" class="collapse">
                            <ul class="nav">
                                <li>
                                    <a href="{{ url('/settings/general') }}">General Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url('/video/encoding') }}">Profile Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url('/settings/profile') }}">Misc Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url('/settings/video-player') }}">Player Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url('/settings/advertisements') }}">Advertise Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url('/settings/firewall') }}">Firewall Settings</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
        <a class="footer" href="https://support.juicycodes.com">
            <i class="fa fa-life-ring"></i>
            <span>SUPPORT CENTER</span>
        </a>
    </div>
    <div class="main">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-btn">
                    <button type="button" class="btn-toggle-fullwidth">
                        <i class="lnr lnr-arrow-left-circle"></i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="sr-only">Toggle Navigation</i>
                        <i class="fa fa-bars icon-nav"></i>
                    </button>
                </div>
                <div id="navbar-menu" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('/blue2/img/profile.png') }}" alt="Avatar" class="img-circle"></img>
                                <span> RJ </span>
                                <i class="icon-submenu lnr lnr-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('user', Auth::user()->username) }}">
                                        <i class="lnr lnr-cog"></i>
                                        <span>Edit Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                        <i class="lnr lnr-exit"></i>
                                        <span>Logout</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="main-content">
            <div class="container-fluid">
             @yield('content')

            </div>
        </div>
        <footer>
            <div class="container-fluid">
                <p class="copyright juicycodes">
                    Designed & Crafted With
                    <i class="fa fa-heart heart"></i> by
                    <a href="https://juicycodes.com">JUICYCODES.COM</a>.
                </p>
            </div>
        </footer>
    </div>
</div>


<script src="{{ asset('/blue2/js/app.js') }}?00"></script>
<script src="{{ asset('/blue2/js/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('/blue2/js/plugins/morris.min.js') }}"></script>
<script src="{{ asset('/blue2/js/plugins/raphael.min.js') }}"></script>
@stack('footer_scripts')
</body>

</html>
