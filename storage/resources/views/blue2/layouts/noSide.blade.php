<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/blue2/css/bootstrap.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/icon-sets.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/waves.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/plugins/sweetalert.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/main.min.css') }}"></link>
    <link rel="stylesheet" href="{{ asset('/blue2/css/app.css?1513618896') }}"></link>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></link>
    <script src="{{ asset('/blue2/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/slimscroll/slimscroll.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/waves/waves.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/plugins/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/theme.min.js') }}"></script>
    <script type="text/javascript">
        var ajax_url = 'http://falcon.streamgo.me/ajax';

    </script>

</head>

</html>
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            @yield('content')
            <div class="juicycodes">
                Made with
                <i class="fa fa-heart heart"></i> by
                <a href="https://juicycodes.com">JUICYCODES.COM</a>
            </div>
        </div>
    </div>
</div>

</body>

</html>
