@extends('blue2.layouts.base')
@section('content')

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Add New Server</h3>
        <div class="right">
            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
        </div>
    </div>
    <form method="post" action="{{ action('ServerController@store') }}">
        <input name="action" type="hidden" class="hide" value="add_server"></input>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-2 col-md-4">
                    <div class="form-group">
                        <label for="server_name" class="control-label">Server Name:</label>
                        <input id="server_name" name="name" type="text" class="form-control" placeholder="Insert a distinct name for this server"></input>
                    </div>
                    <div class="form-group">
                        <label for="server_id" class="control-label">Server ID:</label>
                        <input id="server_id" name="uid" type="text" class="form-control" placeholder="Insert the unique server identity"></input>
                    </div>
                    <div class="form-group">
                        <label for="server_secret" class="control-label">Storage:</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="storage" placeholder="Storage" value="{{ old('storage') }}" />
                            <span class="input-group-addon">MB</span>
                        </div>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4">
                    <div class="form-group">
                        <label for="server_url" class="control-label">Server Domain:</label>
                        <input id="server_url" name="host" type="text" class="form-control" placeholder="Insert the server domain"></input>
                    </div>
                    <div class="form-group">
                        <label for="server_secret" class="control-label">Server Secret:</label>
                        <input id="server_secret" name="secret" type="text" class="form-control" placeholder="Insert the server secret"></input>
                    </div>
                    <div class="form-group">
                        <label for="bandwidth" class="control-label">Bandwidth:</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="bandwidth" placeholder="bandwidth" value="{{ old('bandwidth') }}" />
                            <span class="input-group-addon">MB</span>
                        </div>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4">
                    <div class="form-group">
                        <label for="server_url" class="control-label">Server IP:</label>
                        <input id="server_url" name="ip" type="text" class="form-control" placeholder="Insert the server IP Address"></input>
                    </div>
                    <div class="form-group">
                        <label for="public_url" class="control-label">Public URL:</label>
                        <input id="public_url" name="host" type="url" class="form-control" placeholder="Insert the browseable URL"></input>
                    </div>
                    <div class="form-group">
                        <label for="server_type" class="control-label">Server Type:</label>
                        <select class="form-control" id="server_type" name="server_type">
                            {{-- <option value="s">Storage</option>
                            <option value="s">Encoding</option> --}}
                            <option value="s">Storage + Encoding</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-info">ADD SERVER</button>
        </div>
    </form>
</div>
</div>

@endsection
