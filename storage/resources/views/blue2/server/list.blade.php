@extends('blue2.layouts.base')
@section('content')


                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Manage Servers</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse">
                                    <i class="lnr lnr-chevron-up"></i>
                                </button>
                            </div>
                        </div>
                        <div class="panel-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-striped no-margins">
                                    <thead>
                                        <tr>
                                            <th width="60px">#ID</th>
                                            <th>Name</th>
                                            <th>IP Address</th>
                                            <th>Public URL</th>
                                            <th>Server ID</th>
                                            <th>Server Secret</th>
                                            <th width="120px">Type</th>
                                            <th width="120px">Status</th>
                                            <th width="120px">Added</th>
                                            <th width="120px">Added By</th>
                                            <th width="300px">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($servers as $server)
                                        <tr>
                                            <td>#{{ $server->id }}</td>
                                            <td>{{ $server->name}}</td>
                                            <td>{{ $server->ip }}</td>
                                            <td>{{ $server->url }}</td>
                                            <td>
                                                <span data-toggle="popover" tabindex="0" data-text="{{ $server->uid }}" class="protected">SHOW</span>
                                            </td>
                                            <td>
                                                <span data-toggle="popover" tabindex="0" data-text="{{ $server->secret }}" class="protected">SHOW</span>
                                            </td>
                                            <td>
                                                <div class="badge badge-info">{{ $server->type }}</div>
                                            </td>
                                            <td>
                                                <div class="label label-{{ $server->active ? 'success' : 'warning' }}">{{ $server->active ? 'ACTIVE' : 'DISABLED' }}</div>
                                            </td>
                                            <td>
                                                <span data-toggle="tooltip" title="{{ $server->created_at->toDateTimeString() }}">{{ $server->created_at->toFormattedDateString() }}</span>
                                            </td>
                                            <td>{{ $server->user->username }}</td>
                                            <td>
                                                {{-- TODO Refresh String --}}
                                                <a href="#!" class="btn btn-xs btn-success">
                                                    <i class="fa fa-refresh"></i>
                                                    REFRESH
                                                </a>
                                                {{-- TODO Server Edit --}}
                                                <a href="#!" class="btn btn-xs btn-info">
                                                    <i class="fa fa-pencil"></i>
                                                    EDIT
                                                </a>
                                                <form id="server-disable-{{ $server->id }}" action="{{ url('/server', ['id' => $server->id]) }}" class="" method="POST" hidden>
                                                    {{ csrf_field() }}
                                                    {{ method_field('PATCH') }}
                                                    <input type="hidden" name="active" value="{{ $server->active ? 0 : 1 }}" />
                                                </form>
                                                <a href="#!server-disable-{{ $server->id }}" class="btn btn-xs btn-{{ $server->active ? 'warning' : 'success' }}" onclick="return jc_delete(this);">
                                                    @if ($server->active)
                                                        <i class="fa fa-ban"></i>DISABLE
                                                    @else
                                                        <i class="fa fa-ban"></i>ENABLE
                                                    @endif
                                                </a>
                                                <form id="server-delete-{{ $server->id }}" action="{{ url('/server', ['id' => $server->id]) }}" class="" method="POST" hidden>
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                                <a href="#!server-delete-{{ $server->id }}" class="btn btn-xs btn-danger" onclick="return jc_delete(this);">
                                                    <i class="fa fa-trash"></i>
                                                    DELETE
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-3">
                                    <form method="get">
                                        <div class="input-group m-t-xs">
                                            <input id="query" name="query" type="text" class="form-control input-sm" placeholder="Search server by name or IP..."></input>
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn btn-info btn-sm">Search</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-9 text-right">
                                    {{ $servers->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
@endsection
@push('footer_scripts')
@if (session('status'))
    <script>
        toastr.{{ session('type') ?? 'info' }}('{{ session('status') }}', 'Result', {timeOut: 5000});
    </script>
@endif
@endpush
