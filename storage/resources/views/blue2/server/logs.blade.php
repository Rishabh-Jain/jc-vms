@extends('blue.layouts.base')
@section('content')
                <div class="col-sm-12 manageVideo">
                    <div class="panelHead">
                        <span>Server Logs</span> <a data-toggle="collapse" data-target="#toggleVideo" href="javascript:void"><i class="pull-right fa fa-angle-up"></i></a>
                    </div>

                    <div class="panelBody collapse in" id="toggleVideo">
                        <div class="table-responsive">
                        <table class="table table-striped no-margins table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Message</th>
                                <th scope="col">IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($serverLogs as $serverLog)
                                <tr>
                                    <td>{{ $serverLog->id }}</td>
                                    <td>{{ $serverLog->server->name }}</td>
                                    <td>{{ $serverLog->type }}</td>
                                    <td>{{ $serverLog->log }}</td>
                                    <td>{{ $serverLog->ip }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
