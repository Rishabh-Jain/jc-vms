@extends('blue2.layouts.base')
@section('content')


    <div class="panel">
           <div class="panel-heading">
               <h3 class="panel-title">ADVERTISE SETTINGS</h3>
               <div class="right">
                   <button type="button" class="btn-toggle-collapse">
                       <i class="lnr lnr-chevron-up"></i>
                   </button>
               </div>
           </div>

           <form method="post" action="{{ action('SettingsController@advertisementsStore') }}">
               <input name="action" type="hidden" class="hide" value="save_settings"></input>
               <div class="panel-body">
                   <div class="row">
                       <div class="col-lg-3 col-md-6">
                           <div class="form-group">
                               <label for="pop_ad_opt" class="control-label">Popup AD:</label>
                               <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Whether to show the popup ad code in streaimg & download page"></i>
                               <select class="form-control" id="pop_ad_opt" name="popup_ad">
                                   <option value="n" {{ oldSettings('popup_ad', $settings) == false ? 'selected' : '' }}>Disabled</option>
                                   <option value="y" {{ oldSettings('popup_ad', $settings) == true ? 'selected' : '' }}>Enabled</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label for="pop_ad_code" class="control-label">Popup AD Code:</label>
                               <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Popup ad code to show  in streaimg & download page"></i>
                               <textarea class="form-control" id="pop_ad_code" name="popup_ad_code" rows="6">{{ oldSettings('popup_ad_code', $settings) }}</textarea>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6">
                           <div class="form-group">
                               <label for="banner_ad_opt" class="control-label">Banner AD:</label>
                               <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Whether to show the banner ad code in streaimg page"></i>
                               <select class="form-control" id="banner_ad_opt" name="banner_ad">
                                   <option value="n" {{ oldSettings('banner_ad', $settings) == false ? 'selected' : '' }}>Disabled</option>
                                   <option value="y" {{ oldSettings('banner_ad', $settings) == true ? 'selected' : '' }}>Enabled</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label for="banner_ad_code" class="control-label">Banner AD Code:</label>
                               <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Banner ad code to show  in streaimg page"></i>
                               <textarea class="form-control" id="banner_ad_code" name="banner_ad_code" rows="6"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="panel-footer">
                   {{ csrf_field() }}
                   <button type="submit" class="btn btn-info">SAVE SETTINGS</button>
               </div>
           </form>
       </div>
@endsection
@push('footer_scripts')
    <script type="text/javascript">
        $("#pop_ad_opt").bind("change", function () {
            $value = $(this).val();
            if ($value == "y") {
                $("#pop_ad_code").prop("disabled", false);
            } else {
                $("#pop_ad_code").prop("disabled", true);
            }
        });
        $("#banner_ad_opt").bind("change", function () {
            $value = $(this).val();
            if ($value == "y") {
                $("#banner_ad_code").prop("disabled", false);
            } else {
                $("#banner_ad_code").prop("disabled", true);
            }
        });
        $("#pop_ad_opt").trigger("change");
        $("#banner_ad_opt").trigger("change");
</script>
@if (session('status'))
    <script>
        toastr.success('{{ session('status') }}', 'Saved', {timeOut: 5000});
    </script>
@endif
@endpush
