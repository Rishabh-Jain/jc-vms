@extends('blue2.layouts.base')
@section('content')
<div class="row">
@foreach ($encodings as $encoding)
    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $encoding->type }} Configuration</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse">
                        <i class="lnr lnr-chevron-up"></i>
                    </button>
                </div>
            </div>
            <form method="post" action="{{ action("VideoEncodingController@update", $encoding->id) }}">
                {{ method_field('PUT') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="width" class="control-label">Video Width:</label>
                                <input id="width" name="width" type="text" class="form-control" value="{{ old('width') ?? $encoding->width }}"></input>
                            </div>
                            <div class="form-group">
                                <label for="quality" class="control-label">Quality:</label>
                                <input type="number" class="form-control" id="quality" name="quality" min="1" max="51" value="{{ old('quality') ?? $encoding->quality }}"></input>
                            </div>
                            <div class="form-group">
                                <label for="audio_bit" class="control-label">Audio Bitrate:</label>
                                <input id="audio_bit" name="audio_bit_rate" type="text" class="form-control" value="{{ old('audio_bit_rate') ?? $encoding->audio_bit_rate }}"></input>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="height" class="control-label">Video Height:</label>
                                <input id="height" name="height" type="text" class="form-control" value="{{ old('height') ?? $encoding->height }}"></input>
                            </div>
                            <div class="form-group">
                                <label for="frame_rate" class="control-label">Frame Rate:</label>
                                <input id="frame_rate" name="fps" type="text" class="form-control" placeholder="Leave empty to keep it same" value="{{ old('fps') }}"></input>
                            </div>
                            <div class="form-group">
                                <label for="audio_codec" class="control-label">Audio Codec:</label>
                                <select class="form-control" id="audio_codec" name="audio_codec">
                                    <option value="mp3">mp3</option>
                                    <option value="ac3">ac3</option>
                                    <option value="eac3">eac3</option>
                                    <option value="av_aac" selected="selected">av_aac</option>
                                    <option value="ca_aac">ca_aac</option>
                                    <option value="ca_haac">ca_haac</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info">SAVE PROFILE</button>
                </div>
            </form>
        </div>
    </div>
@endforeach
</div>
@endsection
@push('footer_scripts')
    @if (session('status'))
        <script>
            toastr.success('{{ session('status') }}', 'Saved', {timeOut: 5000});
        </script>
    @endif
@endpush
