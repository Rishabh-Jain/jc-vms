@extends('blue2.layouts.base')
@section('content')


    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">FIREWALL SETTINGS</h3>
            <div class="right">
                <button type="button" class="btn-toggle-collapse">
                    <i class="lnr lnr-chevron-up"></i>
                </button>
            </div>
        </div>

        <form method="post" action="{{ action('SettingsController@firewallStore') }}">
            <input name="action" type="hidden" class="hide" value="save_settings"></input>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="form-group">
                            <label for="accs_restriction" class="control-label">Access Restriction:</label>
                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Disable access from all website except the allowed ones"></i>
                            <select class="form-control" id="accs_restriction" name="accs_restriction">
                                <option value="n" {{ oldSettings('firewall_restriction', $settings) == false ? 'selected' : '' }}>Disable</option>
                                <option value="y" {{ oldSettings('firewall_restriction', $settings) == true ? 'selected' : '' }}>Enable</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="allowed_domains" class="control-label">Allowed Domains:</label>
                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Which website can access video player & downloader page. (,) Separated"></i>
                            <textarea class="form-control" id="allowed_domains" name="firewall_allowed_domains" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="form-group">
                            <label for="blocked_countries" class="control-label">Block Countries:</label>
                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Select Countries that you wish to block from accessing Player/Download"></i>
                            <select name="firewall_countries[]" class="form-control chosen-select" id="blocked_countries" multiple="multiple" data-placeholder="Chose one or more country.You can search.">
                                @foreach ($countries as $country)
                                    <option value="{{ $country->code }}" {{ in_array($country->code, $selected_countries) ? ' selected=selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="blocked_ips" class="control-label">Block Single IP/Subnet:</label>
                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Enter Single IPs/Subnets seperated by new line that you wish to block from accessing Player/Download"></i>
                            <textarea class="form-control" id="blocked_ips" name="firewall_blocked_ips" rows="5"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                    {{ csrf_field() }}
                <button type="submit" class="btn btn-info">SAVE SETTINGS</button>
            </div>
        </form>
    </div>


@endsection
@push('scripts')
    <script src="{{ asset('/blue2/js/plugins/chosen.jquery.min.js') }}"></script>
@endpush
@push('footer_scripts')

<script type="text/javascript">
    // $("#blocked_countries").chosen({
    //     no_results_text: "Oops, nothing found!"
    // });
    $("#accs_restriction").bind("change", function () {
        $value = $(this).val();
        if ($value == "y") {
            $("#allowed_domains").prop("disabled", false);
        } else {
            $("#allowed_domains").prop("disabled", true);
        }
    });
    $("#accs_restriction").trigger("change");
</script>
    @if (session('status'))
        <script>
            toastr.success('{{ session('status') }}', 'Saved', {timeOut: 5000});
        </script>
    @endif

@endpush
