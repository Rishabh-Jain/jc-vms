@extends('blue2.layouts.base') @section('content')
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">GENERAL SETTINGS</h3>
        <div class="right">
            <button type="button" class="btn-toggle-collapse">
                <i class="lnr lnr-chevron-up"></i>
            </button>
        </div>
    </div>

    <form method="post" action="{{ action('SettingsController@generalStore') }}" enctype="multipart/form-data">
        <input name="action" type="hidden" class="hide" value="save_settings"></input>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-2 col-md-3">
                    <div class="form-group">
                        <label for="url" class="control-label">Website URL:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Your Website URL"></i>
                        <input id="url" name="website_url" type="text" class="form-control" value="{{ oldSettings('website_url', $settings) }}"></input>
                    </div>
                    <div class="form-group">
                        <label for="player_slug" class="control-label">Embed Slug:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Slug For Video Embeds Page﻿"></i>
                        <input id="player_slug" name="embed_slug" type="text" class="form-control" value="{{ oldSettings('embed_slug', $settings) }}"></input>
                    </div>
                    <div class="form-group">
                        <label for="job_limit" class="control-label">Syncronized Job:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="How many video will Transfer/Encode/Delete in every Cron Job request?"></i>
                        <input id="job_limit" name="job_limit" type="text" class="form-control" value="{{ oldSettings('sync_jobs', $settings) }}"></input>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <strong>Theme: </strong>
                        </label>
                            <select name="theme" class="form-control">
                                <option value="default"{{ oldSettings('theme', $settings) === 'default' ? ' selected' : ''}}>Default grey</option>
                                <option value="blue" {{ oldSettings('theme', $settings) === 'blue' ? ' selected': '' }}>Blue</option>
                                <option value="blue2" {{ oldSettings('theme', $settings) === 'blue2' ? ' selected': '' }}>Blue 2</option>
                            </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="form-group">
                        <label for="admin_url" class="control-label">Admin Panel URL:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Your Admin Panel URL"></i>
                        <input id="admin_url" name="admin_url" type="text" class="form-control" value="{{ oldSettings('admin_url', $settings) }}"></input>
                    </div>
                    <div class="form-group">
                        <label for="logo" class="control-label">Website Logo:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Your Website Logo URL/File"></i>
                        <span class="pull-right">
                            <span class="label label-info cp" id="pre">
                                <i class="fa fa-cloud-upload"></i>
                            </span>
                        </span>
                        <input id="logo" name="logo" type="url" class="form-control"></input>
                    </div>
                    <div class="form-group">
                        <label for="default_title" class="control-label">Website Title:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Used In Embed & Download Page If Video Title Is Empty"></i>
                        <input id="default_title" name="website_title" type="text" class="form-control" value="{{ oldSettings('website_title', $settings) }}"></input>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-info">SAVE SETTINGS</button>
        </div>
    </form>
</div>
@endsection
@push('footer_scripts')
    <script type="text/javascript">
    $("#pre").click(function () {
        var icon = $(this).find("i");
        $("#logo").fadeOut("fast");
        if (icon.hasClass("fa-cloud-upload")) {
            $(this).fadeOut("fast", function () {
                icon.removeClass("fa-cloud-upload").addClass("fa-chain");
                $("#logo").prop("type", "file").fadeIn("fast");
                $(this).fadeIn("fast");
            });
        } else {
            $(this).fadeOut("fast", function () {
                icon.removeClass("fa-chain").addClass("fa-cloud-upload");
                $("#logo").prop("type", "url").fadeIn("fast");
                $(this).fadeIn("fast");
            });
        }
    });
    $("#rely").bind("change", function () {
        $value = $(this).val();
        if ($value == "core") {
            $("#download_slug").prop("disabled", false);
        } else {
            $("#download_slug").prop("disabled", true);
        }
    });
    $("#rely").trigger("change");
</script>

    @if (session('status'))
        <script>
            toastr.success('{{ session('status') }}', 'Saved', {timeOut: 5000});
        </script>
    @endif

@endpush
