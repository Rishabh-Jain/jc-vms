@extends('blue2.layouts.base')

@section('content')
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">PLAYER SETTINGS</h3>
        <div class="right">
            <button type="button" class="btn-toggle-collapse">
                <i class="lnr lnr-chevron-up"></i>
            </button>
        </div>
    </div>
    <form method="post" action="{{ action('SettingsController@videoPlayerStore') }}">
        <input name="action" type="hidden" class="hide" value="save_settings"></input>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="form-group">
                        <label for="player" class="control-label">Video Player:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Select A Video Player"></i>
                        <select class="form-control" id="player" name="video_player">
                            <option value="videojs" {{ oldSettings( 'video_player', $settings)==='videojs' ? ' selected' : ''}}>Video JS</option>
                            <option value="jwplayer" {{ oldSettings( 'video_player', $settings)==='jwplayer' ? ' selected' : ''}}>JW Player</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="about_text" class="control-label">About Text:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Custom text to display in the right-click menu of JW Player"></i>
                        <input id="about_text" name="jwplayer_about_text" type="text" class="form-control" value="{{ oldSettings('jwplayer_about_text', $settings) }}"></input>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="width" class="control-label">Player Width:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="The desired width of your video player"></i>
                                <input id="width" name="player_width" type="text" class="form-control" value="{{ oldSettings('player_width', $settings) }}"
                                    placeholder="Enter Player Width"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="height" class="control-label">Player Height:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="The desired height of your video player"></i>
                                <input id="height" name="player_height" type="text" class="form-control" value="{{ oldSettings('player_height', $settings) }}"
                                    placeholder="Enter Player Height"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="aspect_ratio" class="control-label">Aspect Ratio:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Enter Aspect Ratio In (x:y) Format"></i>
                                <input id="aspect_ratio" name="aspect_ratio" type="text" value="{{ oldSettings('aspect_ratio', $settings) }}" class="form-control"
                                    placeholder="Enter Aspect Ratio"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="player_controls" class="control-label">Player Controls:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Whether to display the video controls"></i>
                                <select class="form-control" id="player_controls" name="player_controls">
                                    <option value="1" {{ oldSettings( 'aspect_ratio', $settings)==1 ? ' selected' : ''}}>Visible</option>
                                    <option value="0" {{ oldSettings( 'aspect_ratio', $settings)==0 ? ' selected' : ''}}>Hidden</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="autostart" class="control-label">Video Autostart:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Whether the player will attempt to begin playback automatically when a page is loaded"></i>
                                <select class="form-control" id="controls" name="video_autostart">
                                    <option value="1" {{ oldSettings( 'video_autostart', $settings)==1 ? ' selected' : ''}}>Visible</option>
                                    <option value="0" {{ oldSettings( 'video_autostart', $settings)==0 ? ' selected' : ''}}>Hidden</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="primary" class="control-label">Player Rendering Mode:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Sets the default player rendering mode"></i>
                                <select class="form-control" id="primary" name="player_rendering_mode">
                                    <option value="html" {{ oldSettings( 'player_rendering_mode', $settings)==='html' ? ' selected' : ''}}>HTML</option>
                                    <option value="flash" {{ oldSettings( 'player_rendering_mode', $settings)==='flash' ? ' selected' : ''}}>FLASH</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="form-group">
                        <label for="jwp_key" class="control-label">JW Player License Key:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Enter JW Player License Key"></i>
                        <input id="jwp_key" name="jwplayer_licence_key" type="text" class="form-control" value="{{ oldSettings('jwplayer_licence_key', $settings) }}"
                            placeholder="JW Player License Key"></input>
                    </div>
                    <div class="form-group">
                        <label for="about_link" class="control-label">About Link:</label>
                        <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Custom URL to link to when clicking the right-click menu of JW Player"></i>
                        <input id="about_link" name="jwplayer_about_link" type="text" class="form-control" value="{{ oldSettings('jwplayer_about_link', $settings) }}"></input>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font_size" class="control-label">Subtitle Font Size:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Enter Subtitle Font Size"></i>
                                <input id="font_size" name="subtitle_font_size" type="text" class="form-control" value="{{ oldSettings('subtitle_font_size', $settings) }}"
                                    placeholder="Enter Subtitle Font Size"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font_color" class="control-label">Subtitle Font Color:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Can be any hexadecimal color value"></i>
                                <input id="font_color" name="subtitle_font_color" type="text" class="form-control" value="{{ oldSettings('subtitle_font_color', $settings) }}"
                                    placeholder="Enter Subtitle Font Color"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font_family" class="control-label">Subtitle Font Family:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Use this option to change the font family"></i>
                                <input id="font_family" name="subtitle_font_family" type="text" class="form-control" value="{{ oldSettings('subtitle_font_family', $settings) }}"
                                    placeholder="Enter Subtitle Font Family"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="bg_color" class="control-label">Subtitle Background Color:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="This is the highlight color of the text. All HEX color values are accepted"></i>
                                <input id="bg_color" name="subtitle_background_color" type="text" class="form-control" value="{{ oldSettings('subtitle_background_color', $settings) }}"
                                    placeholder="Enter Subtitle Background Color"></input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="share_btn" class="control-label">Share Button:</label>
                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" title="Whether the Share Button will be visible or not"></i>
                                <select class="form-control" id="share_btn" name="jwplayer_share_button">
                                    <option value="1" {{ oldSettings( 'jwplayer_share_button', $settings)==1 ? ' selected' : ''}}>Visible</option>
                                    <option value="0" {{ oldSettings( 'jwplayer_share_button', $settings)==0 ? ' selected' : ''}}>Hidden</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-info">SAVE SETTINGS</button>
        </div>
    </form>
</div>
@endsection

@push('footer_scripts')
    <script type="text/javascript">
        $("#player").bind("change", function () {
            $player = $(this).val();
            if ($player == "jwplayer") {
                $("#jwp_key").prop("disabled", false);
                $("#primary").prop("disabled", false);
                $("#about_text").prop("disabled", false);
                $("#about_link").prop("disabled", false);
                $("#share_btn").prop("disabled", false);
            } else {
                $("#jwp_key").prop("disabled", true);
                $("#primary").prop("disabled", true);
                $("#about_text").prop("disabled", true);
                $("#about_link").prop("disabled", true);
                $("#share_btn").prop("disabled", true);
            }
        });
        $("#player").trigger("change");

    </script>

    @if (session('status'))
        <script>
            toastr.success('{{ session('status') }}', 'Saved', { timeOut: 5000 });
        </script>
    @endif
@endpush
