@extends('blue2.layouts.base')
@section('content')                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Manage Users</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse">
                                <i class="lnr lnr-chevron-up"></i>
                            </button>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-striped no-margins">
                                <thead>
                                    <tr>
                                        <th width="60px">#ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th width="120px">Role</th>
                                        <th width="120px">Status</th>
                                        <th width="120px">Added</th>
                                        <th width="230px">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <div class="label label-{{$user->hasRole('admin') ? 'success' : 'info'}}">{{ $user->getRoleNames()->first() }}</div>
                                        </td>
                                        <td>
                                            <div class="label label-success">{{ $user->active ? 'ACTIVE' : 'InActive'}}</div>
                                        </td>
                                        <td>
                                            <span data-toggle="tooltip" title="{{ $user->created_at->toDateTimeString() }}">{{ $user->created_at->toFormattedDateString() }}</span>
                                        </td>
                                        <td>
                                            <a href="{{ url('user', $user->username) }}" class="btn btn-xs btn-info">
                                                <i class="fa fa-pencil"></i>
                                                EDIT</a>
                                            </a>
                                            {{-- TODO block and delete --}}
                                            <a href="#!" class="btn btn-xs btn-warning">
                                                <i class="fa fa-ban"></i>
                                                BLOCK</a>
                                            </a>
                                            <a href="#!" class="btn btn-xs btn-danger" onclick="return jc_confirm(this);">
                                                <i class="fa fa-trash"></i>
                                                DELETE</a>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-3">
                                <form method="get">
                                    <div class="input-group m-t-xs">
                                        <input id="query" name="query" type="text" class="form-control input-sm" placeholder="Search user by name or email..."></input>
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>

@endsection
