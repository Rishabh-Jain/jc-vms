@extends('blue2.layouts.base')
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Login Log</h3>
            <div class="right">
                <a class="btn btn-danger btn-xs" href="http://falcon.streamgo.me/user/log/clear/">CLEAR LOG</a>
            </div>
        </div>
        <div class="panel-body no-padding">
            <div class="table-responsive">
                <table class="table table-striped no-margins">
                    <thead>
                        <tr>
                            <th width="60px">UID</th>
                            <th>User</th>
                            <th>IP Address</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Timezone</th>
                            <th>Information</th>
                            <th width="120px">Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($userLogs as $log)
                        <tr>
                            <td>
                                <a href="{{ url('user',$log->user->username) }}">#{{ $log->id }}</a>
                            </td>
                            <td>{{ $log->user->username }}</td>
                            <td>{{ $log->ip }}</td>
                            <td>{{ $log->info['countryName'] }}</td>
                            <td>{{ $log->info['cityName'] }}</td>
                            <td></td>
                            <td>
                                <a href="#!" class="btn btn-primary btn-xs more_info" data-info="{{ $log->encode('info') }}">
                                    <i class="fa fa-info-circle"></i>
                                    More Info</a>
                            </td>
                            <td>
                                <span data-toggle="tooltip" title="{{ $log->created_at->toDateTimeString() }}">{{ $log->created_at->toFormattedDateString() }}</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="pull-right">
                {{ $userLogs->links() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal fade" id="more_info" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">More Info</h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-striped no-margins">
                            <tbody>
                                <tr id="base_row" class="hide">
                                    <td>---</td>
                                    <td>---</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer_scripts')
    <script type="text/javascript">
    $(".more_info").click(function () {
        var data = decodeURIComponent($(this).data("info"));
        var modal = $("#more_info");
        modal.find("tr:not(#base_row)").remove();
        $.each($.parseJSON(data), function (k, v) {
            var row = $("#base_row").clone().removeAttr("id");
            row.find("td").eq(1).text(v);
            row.find("td").eq(0).text(k.toUpperCase());
            $(row).appendTo("#more_info tbody").removeClass("hide");
        });
        modal.modal("show");
    });
</script>
@endpush
