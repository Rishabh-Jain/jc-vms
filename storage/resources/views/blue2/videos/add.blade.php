@extends('blue2.layouts.base')
@section('content')

    <div class="row">
        <div class="col-lg-12" id="video_select">
            <form method="post" id="upload_video" action="{{ action("VideoController@store") }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input name="action" type="hidden" class="hide" value="upload_video"></input>
                <div class="row" id="videos">
                    <div class="col-lg-6 col-lg-offset-3 single_video">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Upload Video</h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse">
                                        <i class="lnr lnr-chevron-up"></i>
                                    </button>
                                    <button type="button" class="btn-remove hide">
                                        <i class="lnr lnr-cross"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="video" class="control-label">Select Video:</label>
                                            <span class="pull-right">
                                                <span class="label label-success cp ch" id="pre">
                                                    <i class="fa fa-chain"></i>
                                                </span>
                                            </span>
                                            <input id="video" name="video" type="file" class="form-control video" placeholder="Please enter a valid video url"></input>
                                        </div>
                                        <div class="form-group">
                                            <label for="video_title" class="control-label">Video Title:</label>
                                            <input name="video_title" type="text" class="form-control video_title" placeholder="Please write a video title"></input>
                                        </div>
                                        <label class="fancy-checkbox">
                                            <input name="is_raw" type="checkbox" class="form-control" value="1"></input>
                                            <span>Don't Encode This Video!</span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="subtitles">
                                            <label for="description" class="control-label">Subtitle:</label>
                                            <span class="pull-right">
                                                <span class="label label-info cp" id="add_sub">
                                                    <i class="fa fa-plus"></i>
                                                </span>
                                            </span>
                                            <div class="video-subtitle m-b-xs">
                                                <input name="sub_file[0][]" type="file" class="form-control sub_file"></input>
                                                <input name="sub_label[0][]" type="text" class="form-control sub_label hide" placeholder="Leave it blank to remove this subtitle"></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-info">UPLOAD</button>
                                <button type="button" class="btn btn-info pull-right" id="add_more">ADD MORE</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center hide m-b-md" id="vid_btn">
                        <button type="submit" class="btn btn-info">UPLOAD</button>
                        <button type="button" class="btn btn-info" id="add_more">ADD MORE</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-lg-offset-3 hide" id="video_uploaded">
            <div class="panel">
                <div class="panel-body">
                    <div class="it-icon it-success animate">
                        <div class="it-line it-tip animateSuccessTip"></div>
                        <div class="it-line it-long animateSuccessLong"></div>
                        <div class="it-placeholder"></div>
                        <div class="it-fix"></div>
                    </div>
                    <div class="upload-success m-b-md">Video successfully uploaded!</div>
                    <div class="text-center m-t-sm">
                        <a href="#cancel" class="btn btn-xs btn-success upload_again">
                            <i class="fa fa-cloud-upload"></i>
                            UPLOAD MORE</a>
                        </a>
                        <a href="{{ url('/video') }}" class="btn btn-xs btn-info">
                            <i class="fa fa-bars"></i>
                            MANAGE VIDEOS</a>
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Get Links & Embed Code</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse">
                            <i class="lnr lnr-chevron-up"></i>
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="player_link" class="control-label">Embed Player Link:</label>
                                <input id="player_link" type="text" class="form-control player_link m-b-xs"></input>
                            </div>
                            <div class="form-group">
                                <label for="embed_code" class="control-label">Player Embed Code:</label>
                                <textarea class="form-control embed_code m-b-xs" name="embed_code" rows="1">
                                    <iframe src="" frameborder="0" allowfullscreen="true"></iframe>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-offset-3 hide" id="video_uploading">
            <div class="panel">
                <div class="panel-heading p-xs">
                    <div class="panel-title">UPLOADING
                        <span id="upload_dot">.</span>
                    </div>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse">
                            <i class="lnr lnr-chevron-up"></i>
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="progress-info m-t-sm">Uploaded:
                        <span id="uploaded_p">0%</span> (
                        <span id="uploaded_b">0</span> from
                        <span id="total_size">0</span> )
                        <div class="pull-right">Transfer Rate:
                            <span id="up_speed">0</span> /sec
                        </div>
                    </div>
                    <div class="progress no-margins">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progress" aria-valuenow="0" aria-valuemin="0"
                            aria-valuemax="100" style="width:0%;"></div>
                    </div>
                    <div class="progress-info m-t-xs text-center">Time Left:
                        <span id="time_left">Unknown</span>
                    </div>
                    <div class="text-center">
                        <a href="#cancel" class="btn btn-xs btn-danger cancel_upload">
                            <i class="fa fa-times"></i>
                            CANCEL</a>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    @if($errors->count())
        <script>
        @foreach ($errors->all() as $error)
            toastr.warning('{{ $error }}', 'Error', {timeOut: 5000});
        @endforeach
        </script>
    @endif
    {{-- <script src="{{ asset('/blue2/js/plugins/filesize.min.js') }}"></script>
    <script src="{{ asset('/blue2/js/upload.js') }}"></script>
    <script type="text/javascript">
        $("body").on("click", "#pre", function(){
            var icon = $("#pre").find("i");
            if(icon.hasClass("fa-cloud-upload")){
                $(".video").prop("type", "file");
                icon.removeClass("fa-cloud-upload").addClass("fa-chain");
            } else{
                $(".video").prop("type", "url");
                icon.removeClass("fa-chain").addClass("fa-cloud-upload");
            }
        });
    </script> --}}
@endpush
