@extends('blue2.layouts.base')
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Manage Videos</h3>
            <div class="right">
                <button type="button" class="btn-toggle-collapse">
                    <i class="lnr lnr-chevron-up"></i>
                </button>
            </div>
        </div>
        <div class="panel-body no-padding">
            <div class="table-responsive">
                <table class="table table-striped no-margins">
                    <thead>
                        <tr>
                            <th>Video Title</th>
                            <th width="90px">Qualities</th>
                            <th width="110px">Player Link</th>
                            <th width="110px">Embed Code</th>
                            <th width="80px">Status</th>
                            <th width="80px">Progress</th>
                            <th width="80px">Views</th>
                            <th width="120px">Added</th>
                            <th width="120px">Added By</th>
                            <th width="300px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($videos as $video)
                        <tr>
                            <td>{{ $video->name }}</td>
                            <td>
                                {{-- FIXME Qualities --}}
                                <a href="#!" class="btn btn-info btn-xs more_info" data-info="{{ $video->encodings }}">
                                    <i class="fa fa-superpowers"></i>
                                    Qualities</a>
                            </td>
                            <td>
                                <a href="{{ $video->player_link }}" class="btn btn-success btn-xs" target="_blank">
                                    <i class="fa fa-chain"></i>
                                    Player Link</a>
                            </td>
                            <td>
                                <a href="#!" class="btn btn-primary btn-xs get_embed" data-slug="{{ $video->hash }}">
                                    <i class="fa fa-code"></i>
                                    Get Code</a>
                            </td>
                            <td>
                                <div class="label label-{{ $video->active ? 'success' : 'danger' }}">{{ $video->active ? 'ACTIVE' : 'InACTIVE' }}</div>
                            </td>
                            <td>
                                <span class="badge badge-{{ $video->processed ? 'success' : 'warning' }}">{{ $video->progress }}%</span>
                            </td>
                            <td>
                                <span class="badge badge-info">{{ $video->views }}</span>
                            </td>
                            <td>
                                <span data-toggle="tooltip" title="{{ $video->created_at->toDateTimeString() }}">{{ $video->created_at->toFormattedDateString() }}</span>
                            </td>
                            <td>{{ $video->user->name }}</td>
                            <td>
                                <a href="{{ url('video', $video->id) }}" class="btn btn-xs btn-primary">
                                    <i class="fa fa-info-circle"></i>
                                    DETAILS
                                </a>
                                {{-- TODO create video edit --}}
                                <a href="#!" class="btn btn-xs btn-info">
                                    <i class="fa fa-pencil"></i>
                                    EDIT
                                </a>
                                <form id="video-disable-{{ $video->id }}" action="{{ url('/video', ['id' => $video->id]) }}" class="" method="POST" hidden>
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <input type="hidden" name="active" value="{{ $video->active ? 0 : 1 }}" />
                                </form>
                                <a href="#!video-disable-{{ $video->id }}" class="btn btn-xs btn-{{ $video->active ? 'warning' : 'success' }}" onclick="return jc_delete(this);">
                                    @if ($video->active)
                                        <i class="fa fa-eye-slash"></i>HIDE
                                    @else
                                        <i class="fa fa-eye"></i>SHOW
                                    @endif
                                </a>
                                <form id="video-delete-{{ $video->id }}" action="{{ url('/video', ['id' => $video->id]) }}" class="" method="POST" hidden>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                                <a href="#!video-delete-{{ $video->id }}" class="btn btn-xs btn-danger" onclick="return jc_delete(this);">
                                    <i class="fa fa-trash"></i>
                                    DELETE
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-3">
                    <form method="get">
                        <div class="input-group m-t-xs">
                            <input id="query" name="query" type="text" class="form-control input-sm" placeholder="Search video by title, slug..."></input>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-sm">Search</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-9 text-right">
                    {{ $videos->links() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="embed_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Get Embed Code</h4>
                </div>
                <div class="modal-body p-xs">
                    <textarea class="form-control" id="embed_code" rows="6"></textarea>
                    <textarea class="hide" id="embed_default" rows="6">
                        <iframe src="URL" framewborder="0" allowfullscreen="true"></iframe>
                    </textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="more_info" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Details Of:
                        <small class="vid_name"></small>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-striped no-margins">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Width</th>
                                    <th>Height</th>
                                    <th>Size</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="base_row" class="hide">
                                    <td>---</td>
                                    <td>---</td>
                                    <td>---</td>
                                    <td>---</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer_script')
    <script type="text/javascript">
        $('.get_embed').click(function () {
            var default_iframe = $('#embed_default').val();
            $('#embed_code').val(default_iframe.replace('URL', 'https://streamgo.me/player/' + $(this).data('slug') +
                '/'));
            $('#embed_modal').modal('show');
        });
        $('.table-responsive').on('show.bs.dropdown', function () {
            $('.table-responsive').css('overflow', 'inherit');
        });
        $('.table-responsive').on('hide.bs.dropdown', function () {
            $('.table-responsive').css('overflow', 'auto');
        });

        $(".more_info").click(function () {
            var data = decodeURIComponent($(this).data("info"));
            var modal = $("#more_info");
            modal.find("tbody tr:not(#base_row)").remove();
            $.each($.parseJSON(data), function (k, v) {
                var row = $("#base_row").clone().removeAttr("id");
                row.find("td").eq(0).text(k);
                if (v.name !== "") {
                    modal.find(".vid_name").text(v.name);
                }
                row.find("td").eq(1).text(v.width + "px");
                row.find("td").eq(2).text(v.height + "px");
                row.find("td").eq(3).text(filesize(v.size));
                $(row).appendTo("#more_info tbody").removeClass("hide");
            });
            modal.modal("show");
        });
    </script>
@endpush
