@if (player('video_player') === 'videojs')
    @extends('blue2.videos.players.videojs')
@else
    @extends('blue2.videos.players.jwplayer')
@endif

@push('footer')
    @if (ad('banner_ad'))
        <div class="banner" id="banner">
            <span class="close" onclick="return JuicyCodes.Close(); ">X</span>
            {{ ad('banner_ad_code') }}
        </div>
    @endif
    @if (ad('popup_ad'))
        {{ ad('popup_ad_code') }}
    @endif
@endpush
