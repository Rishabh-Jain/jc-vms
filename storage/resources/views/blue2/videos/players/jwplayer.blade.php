@extends('blue2.videos.players.layout')

@section('content')
<div id="video_player"></div>
@endsection

@push('head')
    <script src="{{ asset('/jwplayer/assets/jwplayer.js') }}"></script>
    <script>
        window.jwplayer.key = "{{ player('jwplayer_licence_key') }}";
    </script>
@endpush
@push('footer')
    @php
        $sources = [];
    @endphp
    @foreach($links->video as $qty => $link)
        @php
            array_push ( $sources, [ 'file' => $video->server->url . $link->play, 'label' => "{$qty}p" ] );
        @endphp
    @endforeach
    <script>
        var player = jwplayer("video_player");
        var config = {
            width: "{{ player('player_width', $video->width) }}",
            height: "{{ player('player_height', $video->height) }}",
            aspectratio: "{{ player('aspect_ratio') }}",
            autostart: "{{ player('video_autostart') }}",
            controls: "{{ player('player_controls') }}",
            primary: "{{ player('video_autostart') }}",
            abouttext: "{{ player('jwplayer_about_text') }}",
            aboutlink: "{{ player('jwplayer_about_link') }}",
            image: "{{ $video->server->url . $links->image }}",
            sources: {!! json_encode($sources, JSON_UNESCAPED_SLASHES) !!},
            // tracks: ' . json_encode($subtitle, JSON_UNESCAPED_SLASHES) . ',
            logo: {
                file: "",
                link: "",
                position: "top-left",
            },
            captions: {
                color: "{{ player('subtitle_font_color') }}",
                fontSize: "{{ player('subtitle_font_size') }}",
                fontFamily: "{{ player('subtitle_font_family') }}",
                backgroundColor: "{{ player('subtitle_background_color') }}",
            }
        };

        @if (player('jwplayer_share_button'))
            config.sharing = {sites: ["facebook","twitter","email","googleplus","reddit"]};
        @endif

        player.setup(config);

    </script>
@endpush
