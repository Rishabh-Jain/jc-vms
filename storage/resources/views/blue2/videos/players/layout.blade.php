<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $video->title }}</title>

    <link rel="stylesheet" href="{{ asset('/jwplayer/assets/juicycodes.css') }}">
    <script src="{{ asset('/jwplayer/assets/juicycodes.js') }}"></script>

    @stack('head')
</head>
<body>
@yield('content')

@stack('footer')
</body>
</html>
