@extends('blue2.videos.players.layout')

@section('content')
<video id="my-video" class="video-js" controls preload="auto" width="{{ player("player_width", $video->width) }}" height="{{ player("player_height", $video->height) }}" poster="{{ $video->server->url . $links->image }}" data-setup="{}" style="margin: auto">
    @foreach($links->video as $qty => $link)
        <source src="{{ $video->server->url . $link->play }}" type='video/mp4' label="{{ $qty }}" res="{{ $qty }}">
    @endforeach
    <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to a web browser that
        <a href="//videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
</video>
@endsection

@push('head')
    <link href="//vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
    <!-- If you'd like to support IE8 -->
    <script src="//vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
@endpush
@push('footer')
    <script src="//vjs.zencdn.net/6.4.0/video.js"></script>
@endpush
