@extends('blue2.layouts.base')
@section('content')

    <div class="row">
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Video Details</h3>
                <div class="right">
                    <span class="badge badge-info">#{{ $video->id }}</span>
                </div>
            </div>
            <div class="panel-body no-padding">
                <table class="table table-striped no-margins top-table">
                    <tr>
                        <td>
                            <b>Title</b>
                        </td>
                        <td align="right" data-toggle="tooltip" title="{{ $video->title }}">{{ $video->title }}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Views</b>
                        </td>
                        <td align="right">
                            <span class="badge badge-info">{{ $video->views }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Added by</b>
                        </td>
                        <td align="right">{{ $video->user->username }}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Uploaded on</b>
                        </td>
                        <td align="right">{{ $video->created_at->toDateTimeString() }}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Unique Video Hash</b>
                        </td>
                        <td align="right">
                            <span class="badge badge-info">{{ $video->hash }}</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Video Progress</h3>
            </div>
            <div class="panel-body no-padding">
                <table class="table table-striped no-margins top-table">
                    <tr>
                        <td>
                            <b>Status</b>
                        </td>
                        <td align="right">
                            <div class="label label-success">ACTIVE</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Progress</b>
                        </td>
                        <td align="right">
                            <span class="badge badge-info">{{ $video->progress }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Video Stage</b>
                        </td>
                        <td align="right">
                            <div class="badge badge-{{ $video->processed ? 'success' : 'info' }}">
                                {{ $video->processed ? 'Completed' : 'Pending' }}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Transferring To</b>
                        </td>
                        <td align="right">
                            <span class="badge badge-success">NULL</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Currently Stored In</b>
                        </td>
                        <td align="right">
                            <span class="badge badge-info">#{{ $video->server->id }} › {{ $video->server->name }}</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Video Actions</h3>
            </div>
            <div class="panel-body padding-md">
                <div class="col-sm-12 text-center p-lg">
                    <div class="form-group m-t-xs">
                        {{-- TODO actions --}}
                        <a href="#!" class="btn btn-info">HIDE VIDEO</a>
                    </div>
                    <div class="form-group">
                        <a href="#!" class="btn btn-success disabled">ADD VIDEO TO QUEUE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Video Log</h3>
        <div class="right">
            <button type="button" class="btn-toggle-collapse">
                <i class="lnr lnr-chevron-up"></i>
            </button>
        </div>
    </div>
    <div class="panel-body no-padding">
        <div class="table-responsive">
            <table class="table table-striped no-margins">
                <thead>
                    <tr>
                        <th width="80px">#ID</th>
                        <th width="80px">#VID</th>
                        <th>Message</th>
                        <th width="180px">Details</th>
                        <th width="80px">Type</th>
                        <th width="240px">Logged In</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($video->logs as $log)
                    <tr>
                        <td>{{ $log->id }}</td>
                        <td>{{ $video->id }}</td>
                        <td>{{ $log->log }}</td>
                        <td>
                            <a href="#!" class="btn btn-info btn-xs more_info" data-id="log_113680">
                                <i class="fa fa-superpowers"></i>
                                Details</a>
                            </td>
                            <td>
                                <div class="label label-info">INFO</div>
                            </td>
                            <td>{{ $log->created_at->toDateTimeString() }}</td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="more_info" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">More Details</h4>
            </div>
            <div class="modal-body no-padding">
                <pre class="json-renderer"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer_scripts')

<script src="http://falcon.streamgo.me/assets/js/plugins/json.js"></script>

<script type="text/javascript">
    var info = { "log_113680": { "base": "done", "status": "success", "free_space": { "current": 993, "storage": 1411 } }, "log_113679": { "base": "1080P", "status": "success", "free_space": { "current": 993, "storage": 1411 } }, "log_113678": { "base": "720P", "status": "success", "free_space": { "current": 992, "storage": 1412 } }, "log_113677": { "base": "480P", "status": "success", "free_space": { "current": 991, "storage": 1413 } }, "log_113676": { "base": "360P", "status": "success", "free_space": { "current": 991, "storage": 1413 } }, "log_113675": null, "log_113674": null, "log_113673": { "pixel": "1080" }, "log_113672": null, "log_113671": { "pixel": "720" }, "log_113670": null, "log_113669": { "pixel": "480" }, "log_113668": null, "log_113663": { "pixel": "360" }, "log_113661": { "status": "success", "details": { "width": 1920, "height": 1080, "duration": null }, "free_space": 981, "server": { "id": "3", "name": "ENCODER-1", "url": "http:\/\/62.210.246.99", "domain": "https:\/\/encoder1.streamcherry.xyz", "uid": "encoder-server-1", "secret": "caEQprUtXlDpor6", "space": "984", "type": "2", "status": "1", "added": "2017-07-25 20:00:44", "added_by": "2" } }, "log_113660": { "orginal": "\/home\/streamgo\/public_html\/videos\/ad81777dad3bcf92141aab884004b94f.jc", "subtitles": [] }, "log_113658": null, "log_113657": null };
    $(".more_info").click(function () {
        var id = $(this).data("id");
        var modal = $("#more_info");
        var body = modal.find(".json-renderer");
        body.jsonBrowse(info[id]);
        console.log(info[id]);
        modal.modal("show");
    });
</script>

@endpush
