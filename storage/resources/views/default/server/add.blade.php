@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Add Server
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 col-md-9">
                            @if(!empty($created))
                                <div class="alert alert-success">The server has been created.</div>
                            @endif
                            <form class="m-3" method="post" action="{{ action("ServerController@store") }}">
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Name : </strong>
                                    </label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="name" placeholder="Server Name" value="{{ old('name') }}"/>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>ID : </strong>
                                    </label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="uid" placeholder="Server ID" value="{{ old('uid') }}"/>
                                        @if ($errors->has('uid'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('uid') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Access Key : </strong>
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="secret" value="{{ old('secret') }}" placeholder="Server Key"/>
                                        @if ($errors->has('secret'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('secret') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Domain : </strong>
                                    </label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="host" placeholder="Server Domain" value="{{ old('host') }}"/>
                                        @if ($errors->has('host'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('host') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label class="col-form-label col-sm-2 text-right">
                                        <strong>Server Ip : </strong>
                                    </label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="ip" placeholder="Server ip" value="{{ old('ip') }}"/>
                                        @if ($errors->has('ip'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ip') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Storage : </strong>
                                    </label>
                                    <div class="input-group col-sm-4">
                                        <input type="number" class="form-control" name="storage" placeholder="Storage" value="{{ old('storage') }}"/>
                                        <span class="input-group-addon">MB</span>
                                        @if ($errors->has('storage'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('storage') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label class="col-form-label col-sm-2 text-sm-right">
                                        <strong>Bandwidth : </strong>
                                    </label>
                                    <div class="input-group col-sm-4">
                                        <input type="number" class="form-control" name="bandwidth" placeholder="Bandwidth" value="{{ old('bandwidth') }}"/>
                                        <span class="input-group-addon">MB</span>
                                        @if ($errors->has('bandwidth'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bandwidth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Public URL : </strong>
                                    </label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control" name="uri" placeholder="Public URL"/>
                                        @if ($errors->has('uri'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('uri') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label class="col-form-label col-sm-2 text-sm-right">
                                        <strong>Type : </strong>
                                    </label>
                                    <div class="input-group col-sm-4">
                                        <select name="type" class="form-control custom-select">
                                            <option value="s">Storage + Encoding</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Add Server</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
