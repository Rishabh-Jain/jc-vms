@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header">
                    Server Logs
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Message</th>
                                <th scope="col">IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($serverLogs as $serverLog)
                                <tr>
                                    <td>{{ $serverLog->id }}</td>
                                    <td>{{ $serverLog->server->name }}</td>
                                    <td>{{ $serverLog->type }}</td>
                                    <td>{{ $serverLog->log }}</td>
                                    <td>{{ $serverLog->ip }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
