@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-9 my-3 mx-auto">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Advertisements Settings
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            @if (session('status'))
                                <div class="alert alert-success my-2">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action("SettingsController@advertisementsStore") }}">

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Popup ads: </strong>
                                    </label>
                                    <div class="col">
                                        <select name="popup_ad" class="form-control">
                                            <option value="n" {{ old('popup_ad', optional($settings->first( function ($value, $key) { return $value['name'] === 'popup_ad'; } ))->value == false) ? 'selected' : '' }}>Disabled</option>
                                            <option value="y" {{ old('popup_ad', optional($settings->first( function ($value, $key) { return $value['name'] === 'popup_ad'; } ))->value == true) ? 'selected' : '' }}>Enabled</option>
                                        </select>
                                        @if ($errors->has('popup_ad'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('popup_ad') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Popup ads code: </strong>
                                    </label>
                                    <div class="col">
                                        <textarea class="form-control" name="popup_ad_code" placeholder="Popup Adds" >{{ old('popup_ad_code', optional($settings->first( function ($value, $key) { return $value['name'] === 'popup_ad_code'; } ))->value) }}</textarea>
                                        @if ($errors->has('popup_ad_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('popup_ad_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Banner ads: </strong>
                                    </label>
                                    <div class="col">
                                        <select name="banner_ad" class="form-control">
                                            <option value="n" {{ old('banner_ad', optional($settings->first( function ($value, $key) { return $value['name'] === 'banner_ad'; } ))->value == false) ? 'selected' : '' }}>Disabled</option>
                                            <option value="y" {{ old('banner_ad', optional($settings->first( function ($value, $key) { return $value['name'] === 'banner_ad'; } ))->value == true) ? 'selected' : '' }}>Enabled</option>
                                        </select>
                                        @if ($errors->has('banner_ad'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('banner_ad') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Banner ads code: </strong>
                                    </label>
                                    <div class="col">
                                        <textarea class="form-control" name="banner_ad_code" placeholder="Popup Adds" >{{ old('banner_ad_code', optional($settings->first( function ($value, $key) { return $value['name'] === 'banner_ad_code'; } ))->value) }}</textarea>
                                        @if ($errors->has('banner_ad_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('banner_ad_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
