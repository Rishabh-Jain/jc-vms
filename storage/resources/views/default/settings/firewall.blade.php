@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-9 my-3 mx-auto">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Advertisements Settings
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            @if (session('status'))
                                <div class="alert alert-success my-2">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action("SettingsController@advertisementsStore") }}">

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Firewall Restrictions: </strong>
                                    </label>
                                    <div class="col">
                                        <select name="firewall_restriction" class="form-control">
                                            <option value="n" {{ old('firewall_restriction', optional($settings->first( function ($value, $key) { return $value['name'] === 'firewall_restriction'; } ))->value == false) ? 'selected' : '' }}>Disabled</option>
                                            <option value="y" {{ old('firewall_restriction', optional($settings->first( function ($value, $key) { return $value['name'] === 'firewall_restriction'; } ))->value == true) ? 'selected' : '' }}>Enabled</option>
                                        </select>
                                        @if ($errors->has('firewall_restriction'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firewall_restriction') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Blocked Contries: </strong>
                                    </label>
                                    <div class="col">
                                        <select name="firewall_countries" class="form-control" multiple>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->code }}" {{ in_array($country->code, $selected_countries) ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                        </select>
                                        @if ($errors->has('firewall_countries'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firewall_countries') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Allowed Domains: </strong>
                                    </label>
                                    <div class="col">
                                        <textarea class="form-control" name="firewall_allowed_domains" placeholder="Allowed domains comma seperated." >{{ old('firewall_allowed_domains', optional($settings->first( function ($value, $key) { return $value['name'] === 'firewall_allowed_domains'; } ))->value) }}</textarea>
                                        @if ($errors->has('firewall_allowed_domains'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firewall_allowed_domains') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Blocked IPs/Subnet</strong>
                                    </label>
                                    <div class="col">
                                        <textarea class="form-control" name="firewall_blocked_ips" placeholder="Banned IPs seperated by line brakes." >{{ old('firewall_blocked_ips', optional($settings->first( function ($value, $key) { return $value['name'] === 'firewall_blocked_ips'; } ))->value) }}</textarea>
                                        @if ($errors->has('firewall_blocked_ips'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firewall_blocked_ips') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
