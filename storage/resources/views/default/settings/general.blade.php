@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Add Video
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            @if (session('status'))
                                <div class="alert alert-success my-2">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action("SettingsController@generalStore") }}">
                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Website URL: </strong>
                                    </label>
                                    <div class="col-10">
                                        <input class="form-control" name="website_url" placeholder="Website URL" value="{{ oldSettings('website_url', $settings) }}"/>
                                        @if ($errors->has('website_url'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('website_url') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Theme: </strong>
                                    </label>
                                    <div class="col-10">
                                        <select name="theme" class="form-control">
                                            <option value="default"{{ oldSettings('theme', $settings) === 'default' ? ' selected' : ''}}>Default grey</option>
                                            <option value="blue" {{ oldSettings('theme', $settings) === 'blue' ? ' selected': '' }}>Blue</option>
                                        </select>
                                        @if ($errors->has('theme'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('theme') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Admin URL: </strong>
                                    </label>
                                    <div class="col-10">
                                        <input class="form-control" name="admin_url" placeholder="Admin URL" value="{{ oldSettings('admin_url', $settings) }}"/>
                                        @if ($errors->has('admin_url'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('admin_url') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Embed Slug</strong>
                                    </label>
                                    <div class="col-10">
                                        <input class="form-control" name="embed_slug" placeholder="Embed Slug" value="{{ oldSettings('embed_slug', $settings) }}"/>
                                        @if ($errors->has('embed_slug'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('embed_slug') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Website Title: </strong>
                                    </label>
                                    <div class="col-10">
                                        <input class="form-control" name="website_title" placeholder="Website Title" value="{{ oldSettings('website_title', $settings) }}"/>
                                        @if ($errors->has('website_title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('website_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-2 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
