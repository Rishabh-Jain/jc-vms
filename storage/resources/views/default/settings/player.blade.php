@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-12 my-3 mx-auto">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Profile Settings
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            @if (session('status'))
                                <div class="alert alert-success my-2">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action("SettingsController@videoPlayerStore") }}">
                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Player Type</strong>
                                    </label>
                                    <div class="col-3">
                                        <select name="video_player" class="form-control">
                                            <option value="videojs">Video JS</option>
                                            <option value="jwplayer">JW Player</option>
                                        </select>
                                        @if ($errors->has('video_player'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('video_player') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>JW Player key</strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="jwplayer_licence_key" placeholder="JWPLAYER Licence" value="{{ old('jwplayer_licence_key', optional($settings->first( function ($value, $key) { return $value['name'] === 'jwplayer_licence_key'; } ))->value) }}"/>
                                        @if ($errors->has('jwplayer_licence_key'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('jwplayer_licence_key') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>About Text: </strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="jwplayer_about_text" placeholder="About Text" value="{{ old('jwplayer_about_text', optional($settings->first( function ($value, $key) { return $value['name'] === 'jwplayer_about_text'; } ))->value) }}"/>
                                        @if ($errors->has('jwplayer_about_text'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('jwplayer_about_text') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>About Link: </strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="jwplayer_about_link" placeholder="About Link" value="{{ old('jwplayer_about_link', optional($settings->first( function ($value, $key) { return $value['name'] === 'jwplayer_about_link'; } ))->value) }}"/>
                                        @if ($errors->has('jwplayer_about_link'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('jwplayer_about_link') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Player Width: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="player_width" placeholder="Player Width" value="{{ old('player_width', optional($settings->first( function ($value, $key) { return $value['name'] === 'player_width'; } ))->value) }}"/>
                                        @if ($errors->has('player_width'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('player_width') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Player Height: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="player_height" placeholder="Player Height" value="{{ old('player_height', optional($settings->first( function ($value, $key) { return $value['name'] === 'player_height'; } ))->value) }}"/>
                                        @if ($errors->has('player_height'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('player_height') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Subtitle font size: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="subtitle_font_size" placeholder="Font size" value="{{ old('subtitle_font_size', optional($settings->first( function ($value, $key) { return $value['name'] === 'subtitle_font_size'; } ))->value) }}"/>
                                        @if ($errors->has('subtitle_font_size'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subtitle_font_size') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Subtitle Font Color: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="subtitle_font_color" placeholder="Font color" value="{{ old('subtitle_font_color', optional($settings->first( function ($value, $key) { return $value['name'] === 'subtitle_font_color'; } ))->value) }}"/>
                                        @if ($errors->has('subtitle_font_color'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subtitle_font_color') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Subtitle Font Family: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="subtitle_font_family" placeholder="Font Family" value="{{ old('subtitle_font_family', optional($settings->first( function ($value, $key) { return $value['name'] === 'subtitle_font_family'; } ))->value) }}"/>
                                        @if ($errors->has('subtitle_font_family'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subtitle_font_family') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Sub Background: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="subtitle_background_color" placeholder="Background color" value="{{ old('subtitle_background_color', optional($settings->first( function ($value, $key) { return $value['name'] === 'subtitle_background_color'; } ))->value) }}"/>
                                        @if ($errors->has('subtitle_background_color'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subtitle_background_color') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Aspact Ratio: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="text" class="form-control" name="player_width" placeholder="Aspact ratio" value="{{ old('aspact_ratio', optional($settings->first( function ($value, $key) { return $value['name'] === 'aspact_ratio'; } ))->value) }}"/>
                                        @if ($errors->has('aspact_ratio'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('aspact_ratio') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Player Controls</strong>
                                    </label>
                                    <div class="col">
                                        <select name="player_controls" class="form-control">
                                            <option value="1">Visible</option>
                                            <option value="0">Hidden</option>
                                        </select>
                                        @if ($errors->has('player_controls'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('player_controls') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-form-label col-2">
                                        <strong>Video Autostrat</strong>
                                    </label>
                                    <div class="col">
                                        <select name="video_autostart" class="form-control">
                                            <option value="1">Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                        @if ($errors->has('video_autostart'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('video_autostart') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-2">
                                        <strong>Share button</strong>
                                    </label>
                                    <div class="col-2">
                                        <select name="jwplayer_share_button" class="form-control">
                                            <option value="1">Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                        @if ($errors->has('jwplayer_share_button'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('jwplayer_share_button') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
