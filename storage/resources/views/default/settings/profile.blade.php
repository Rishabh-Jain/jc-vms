@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-9 my-3 mx-auto">
            <div class="card text-bold">
                <div class="card-header bg-dark text-light">
                    Profile Settings
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            @if (session('status'))
                                <div class="alert alert-success my-2">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="m-3" method="post" enctype="multipart/form-data" action="{{ action("SettingsController@profileStore") }}">
                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Default Preview Link: </strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="default_priview_link" placeholder="Default Preview Link" value="{{ old('default_priview_link', optional($settings->first( function ($value, $key) { return $value['name'] === 'default_priview_link'; } ))->value) }}"/>
                                        @if ($errors->has('default_priview_link'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('default_priview_link') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Default Video Link: </strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="default_video_link" placeholder="Default Video Link" value="{{ old('default_video_link', optional($settings->first( function ($value, $key) { return $value['name'] === 'default_video_link'; } ))->value) }}"/>
                                        @if ($errors->has('default_video_link'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('default_video_link') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Items Per Page: </strong>
                                    </label>
                                    <div class="col">
                                        <input type="number" class="form-control" name="items_per_page" placeholder="Items Per Page" value="{{ old('items_per_page', optional($settings->first( function ($value, $key) { return $value['name'] === 'items_per_page'; } ))->value) }}"/>
                                        @if ($errors->has('items_per_page'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('items_per_page') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-3">
                                        <strong>Video Sub Title: </strong>
                                    </label>
                                    <div class="col">
                                        <input class="form-control" name="video_sub_titles" placeholder="Video Sub Title" value="{{ old('video_sub_titles', optional($settings->first( function ($value, $key) { return $value['name'] === 'video_sub_titles'; } ))->value) }}"/>
                                        @if ($errors->has('video_sub_titles'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('video_sub_titles') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-3 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Save Settings</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
