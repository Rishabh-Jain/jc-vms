@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header bg-dark text-light">
                    User Logs
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Username</th>
                                <th scope="col">Type</th>
                                <th scope="col">Message</th>
                                <th scope="col">IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($userLogs as $userLog)
                                <tr>
                                    <td>{{ $userLog->id }}</td>
                                    <td>{{ $userLog->user->name }}</td>
                                    <td><a href="{{ url("user", $userLog->user->username) }}">{{ $userLog->user->username }}</a></td>
                                    <td>{{ $userLog->type }}</td>
                                    <td>{{ $userLog->log }}</td>
                                    <td>{{ $userLog->ip }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
