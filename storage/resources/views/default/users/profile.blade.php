@extends('layouts.base')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card text-dark text-center mt-3">
                <div class="card-header text-white bg-dark pb-1">
                    <h3><strong>Profile Edit</strong></h3>
                </div>
                <div class="card-block pt-3 bg-light">
                    <form method="POST" action="{{ url('user', $user->username) }}">
                        {{ csrf_field() }}

                        <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-4 col-form-label">Name</label>

                            <div class="col-sm-7">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ?? $user->name }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 col-form-label">E-Mail Address</label>

                            <div class="col-sm-7">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') ?? $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-sm-4 col-form-label">Username</label>

                            <div class="col-sm-7">
                                <input id="username" class="form-control" name="username" placeholder="{{ $user->username }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 col-form-label">Password</label>

                            <div class="col-sm-7">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <hr />
                        <div class="form-group row{{ $errors->has('password_old') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 col-form-label">Current Password</label>

                            <div class="col-sm-7">
                                <input id="password_old" type="password" class="form-control" name="password_old" required>

                                @if ($errors->has('password_old'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_old') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-7">
                                <button type="submit" class="btn btn-dark btn-block" disabled>
                                    Login
                                </button>

                                {{--<a class="btn btn-link text-white" href="{{ route('password.request') }}">--}}
                                    {{--Forgot Your Password?--}}
                                {{--</a>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
