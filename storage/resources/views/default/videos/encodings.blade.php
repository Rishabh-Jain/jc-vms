@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header bg-dark text-light">
                    List Encodings
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Canvas</th>
                                <th scope="col">Frame Rate</th>
                                <th scope="col">Audio Encoding</th>
                                <th scope="col">Audio Codac</th>
                                <th scope="col">Params</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($encodings as $encoding)
                                <tr>
                                    <td>{{ $encoding->id }}</td>
                                    <td>{{ $encoding->name }}</td>
                                    <td>{{ $encoding->canvas }}</td>
                                    <td>{{ $encoding->fps }}</td>
                                    <td>{{ $encoding->audio_bit_rate }}</td>
                                    <td class="text-muted">{{ $encoding->audio_codec }}</td>
                                    <td>{{ $encoding->params }}</td>
                                    <td>
                                        <a class="btn btn-outline-primary btn-sm" href="{{ route("encoding.edit", $encoding->id)  }}"><span class="glyphicon glyphicon-pencil"></span>Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
