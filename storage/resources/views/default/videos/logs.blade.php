@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header">
                    Videos Logs
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead">
                            <tr>
                                <th scope="col">#id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Message</th>
                                <th scope="col">IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($videoLogs as $videoLog)
                                <tr>
                                    <td>{{ $videoLog->id }}</td>
                                    <td>{{ $videoLog->video->title }}</td>
                                    <td>{{ $videoLog->type }}</td>
                                    <td>{{ $videoLog->log }}</td>
                                    <td>{{ $videoLog->ip }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
