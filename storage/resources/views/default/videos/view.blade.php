@extends('layouts.base') @section('content')
<div class="card my-3 col-3 p-0">
    <div class="card-header bg-dark text-light align-middle">
        Video details
        <span class="badge bg-info pull-right">{{ $video->id }}</span>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped m-0 p-0">
            <tbody>
                <tr>
                    <td>
                        <b>Title</b>
                    </td>
                    <td align="right" data-toggle="tooltip" title="" data-original-title="{{ $video->title }}">{{ $video->title }}</td>
                </tr>
                <tr>
                    <td>
                        <b>Views</b>
                    </td>
                    <td align="right">
                        <span class="badge badge-info"> {{ $video->views }}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Added by</b>
                    </td>
                    <td align="right">{{ $video->user->username }}</td>
                </tr>
                <tr>
                    <td>
                        <b>Uploaded on</b>
                    </td>
                    <td align="right">{{ $video->created_at->toDateTimeString() }}</td>
                </tr>
                <tr>
                    <td>
                        <b>Unique Video Hash</b>
                    </td>
                    <td align="right">
                        <span class="badge badge-info">{{ $video->hash }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
