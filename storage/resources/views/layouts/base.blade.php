<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url( "/" ) }}">{{ config('app.name') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topNavContent" aria-controls="topNavContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="topNavContent">
            <ul class="navbar-nav mr-auto">
                @if (Auth::check())
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url( "/" ) }}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ url( "/" ) }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Servers
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url( "server/create" ) }}">New</a>
                        <a class="dropdown-item" href="{{ url( "server" ) }}">Manage</a>
                        <a class="dropdown-item" href="{{ url( "server/logs" ) }}">Logs</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Video
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url( "video/create" ) }}">New</a>
                        <a class="dropdown-item" href="{{ url( "video" ) }}">Manage</a>
                        <a class="dropdown-item" href="{{ url( "video/encoding" ) }}">Encodings</a>
                        <a class="dropdown-item" href="{{ url( "video/logs" ) }}">Logs</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Video Encodings
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url( "video/encoding" ) }}">List</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Users
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url( "/register" ) }}">Add User</a>
                        <a class="dropdown-item" href="{{ url( "/users/list" ) }}">Manage Users</a>
                        <a class="dropdown-item" href="{{ url( "/users/logs" ) }}">Manage User Logs</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Settings
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url( "/settings/general" ) }}">General Settings</a>
                        <a class="dropdown-item" href="{{ url( "/settings/profile" ) }}">Profile Settings</a>
                        <a class="dropdown-item" href="{{ url( "/settings/advertisements" ) }}">Advertisements Settings</a>
                        <a class="dropdown-item" href="{{ url( "/settings/firewall" ) }}">Firewall Settings</a>
                        <a class="dropdown-item" href="{{ url( "/settings/video-player" ) }}">Player Settings</a>
                    </div>
                </li>
                @endif
            </ul>
            <ul class="navbar-nav my-2 my-lg-0">
                @if (Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                @endIf
            </ul>
        </div>
    </nav>
    <!-- Scripts -->
    <div class="container-fluid">
        @yield('content')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
