<?php

Artisan::command("install {--D|sampledata} {--S|settings}", function () {
    $this->info('Installing...');
    $this->call('migrate:fresh');

    $this->call('install:addAdminRoles');
    $this->call('install:addAdmin');

    if ($this->option('sampledata')) {
        $this->call('install:sampledata');
    }

    if ($this->option('settings')) {
        $this->call('install:settings');
    }

    $this->info("Software installed successfully.");

});
