<?php

Route::redirect('/','/dashboard');

// Login Routes
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Dashboard Routes
    Route::get('dashboard', 'DashboardController@index');

    // Settings Routes
    Route::get('settings/general', 'SettingsController@general');
    Route::get('settings/advertisements', 'SettingsController@advertisements');
    Route::get('settings/profile', 'SettingsController@profile');
    Route::get('settings/firewall', 'SettingsController@firewall');
    Route::get('settings/video-player', 'SettingsController@videoPlayer');

    Route::post('settings/general', 'SettingsController@generalStore');
    Route::post('settings/advertisements', 'SettingsController@advertisementsStore');
    Route::post('settings/profile', 'SettingsController@profileStore');
    Route::post('settings/firewall', 'SettingsController@firewallStore');
    Route::post('settings/video-player', 'SettingsController@videoPlayerStore');

    // Users Routes
    Route::get('users/list', 'DashboardController@users');
    Route::get('users/logs', 'DashboardController@userLogs');
    Route::get('user/{user}', "DashboardController@profile")->middleware('owner:account');
    Route::post('user/{user}', "DashboardController@updateUser")->middleware('owner:account');

    // Server Routes
    Route::get('server/logs', 'ServerController@logs');
    Route::resource('server', 'ServerController');

    // Video Routes
    Route::resource('video/encoding', 'VideoEncodingController');
    Route::get('video/link/{hash}', 'VideoController@link');
    Route::get('video/logs', 'VideoController@logs');
    Route::resource('video', 'VideoController');

});
